## [3.5.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v3.4.0...v3.5.0) (2025-02-20)


### Features

* add playbooks to collection ([048eb17](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/048eb17100a75f900af4db4cad66e9560db415cd))
* **docker_compose:** add option to exclude services from dc-backup ([8065cfb](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/8065cfbb85a4bd06b61c8c6ef3dc4cb02c0cfba6))
* **docker_compose:** add variable and cronjob to start containers after reboot ([e115cdc](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/e115cdc5d9ab14ffecba325cd15d28a37b04c5eb))
* format roles with yq-sort ([c33614c](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/c33614ca2a364ec0e7de082ef7eedee3043ffb76))
* **hetzner_hcloud:** add playbook and info about dynamic inventory ([4645b34](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/4645b344747619c7dce68d927fa61036825a465d))
* **inwx_dns:** add playbook, add more default values, update dependency to 1.3.1 ([937b2b0](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/937b2b097f1ff95d0c1149a82fc631f28feaadb3))
* **linux_network:** add more robust executable checking for netplan ([a5b44c9](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/a5b44c9f53aa113371b0d80ebd18c14f43c34075))
* **linux_network:** add netplan as network backend ([0f77b3c](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/0f77b3c871c0c19dde28070346225565d9662552))
* **linux_root:** follow symlinks for authorized_keys ([76f5d25](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/76f5d25495abdbd67eb2b67b2e8583a16a34c21f))
* **linux_root:** use variables for bashrc and vimrc content ([3a6f3dd](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/3a6f3ddc0920498a3abd084b2f58677fa9ab4406))
* **nvidia_driver:** create separate playbook ([a204841](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/a204841148097b2f095fae97dfe6c195958553c0))
* **nvidia_driver:** remove backports kernel, create docker config directory ([5c8adbf](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/5c8adbf72a7bc62e851971be7981cd36acb95445))

## [3.4.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v3.3.0...v3.4.0) (2024-08-02)


### Features

* **linux_network:** stop and disable systemd-networkd.service ([472a224](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/472a224f096a1798d81b55eb6e9ead6404761461))
* **linux_packages:** add more tools ([1ec32f0](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/1ec32f06b2830bae840a330ec6de903d8eefed72))

## [3.3.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v3.2.0...v3.3.0) (2024-07-17)


### Features

* **linux_network:** revert to ifupdown2, remove cloud-init network config ([5d83b02](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/5d83b026907d2014f36e909adf92033bcf7c6aa4))
* **linux_packages:** remove media packages, add miscellaneous tools ([1b1b4dd](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/1b1b4dd7d264ec73105a943e3a832b49c5da9ce9))

## [3.2.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v3.1.0...v3.2.0) (2024-07-05)


### Features

* **linux_network:** install ifupdown by default ([043739f](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/043739f954f0e102fdcceba5f7107d20d867c79a))

## [3.1.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v3.0.0...v3.1.0) (2024-06-05)


### Features

* **k3s_setup:** add os specific files ([0bd3a5f](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/0bd3a5f43dddca9373b42d7797de7a3ff55a9b3d))
* **k3s_setup:** rewrite k3s_setup role ([80cdb61](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/80cdb61731de09fd4d1aea4682bdc7b2e65e1154))
* **pve_vm:** add variable for cpu type ([fdb7a8c](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/fdb7a8c8eacfc648a5c48b4dcc75efe9bd172751))

## [3.0.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v2.10.0...v3.0.0) (2024-03-26)


### ⚠ BREAKING CHANGES

* remove prefix 'omnitoolkit_general__' from all variables
* drop support for debian 11
* **tailscale_setup:** rename role tailscale to tailscale_setup
* **k3s_setup:** rename role k3s to k3s_setup

### Features

* drop support for debian 11 ([d5141a9](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/d5141a9a8db2816d6f0b0818c49a255959fce323))
* **k3s_setup:** rename role k3s to k3s_setup ([c7d3945](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/c7d3945ae21ccef6a44776e24b7884e03ba92c67))
* remove prefix 'omnitoolkit_general__' from all variables ([6195744](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/6195744b19721c1c7915d9339ddb16d48bec3aad))
* **tailscale_setup:** rename role tailscale to tailscale_setup ([030fc8e](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/030fc8ecdd8fae312a12fc8dbb62a17e19f5ed1f))

## [2.10.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v2.9.0...v2.10.0) (2024-03-26)


### Features

* add robust systemd service checking ([5c674e3](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/5c674e34e78deb59d0dfc7d54e3ae6b795516ee5))
* add suggestions from yq-sort and ansible-lint ([300ef35](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/300ef3582ffcd73051c729fd42771aab90227505))
* **docker_compose:** add os specific files ([7f48287](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/7f482873cde13909cb77e17f4e65e3e8f3171bed))
* **docker_compose:** migrate to compose v2 ([504cb5a](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/504cb5a11b3e7a531716b29c0591c19073565dca))
* **linux_network:** don't restart network when changing rp_filter ([d6223d1](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/d6223d11a981c37326c3ef16d7d2d704698ccbe9))
* **linux_network:** set network name policy ([7de49cb](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/7de49cb7b7e9387393ab552cc6b3f082c8c5b5a7))
* **linux_packages:** explicitly install epel-release for centos 7 ([a6778e5](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/a6778e5db1cae31ed28a9673b18969714feddf79))
* **nvidia_driver:** install nvtop ([b3bd9b8](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/b3bd9b811fe4dd199956c22612fa3bd800c9e285))


### Bug Fixes

* **docker_compose:** check for docker command ([d81ae0e](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/d81ae0e35cd08b01af3a1545f6235bb7eac6aaf6))
* **docker_setup:** use ansible_architecture instead of ansible_kernel ([1a0c1d0](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/1a0c1d05489e31c69d5bad95cd661ec8e2b1a7e0))


### Tests

* restructure test variables and playbooks ([7a7166a](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/7a7166a92f79095bbb4b47dc1c2fa88af0346b96))

## [2.9.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v2.8.1...v2.9.0) (2024-03-13)


### Features

* add helper scripts ([8df36dd](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/8df36ddf74513a4fdeafbb3c8811dbeeac1f4f15))
* **docker_compose:** change default variables for backup and source path ([637e131](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/637e131394b2900bade29717faf0c35cb2f0c757))
* **nvidia_driver:** add new role to configure nvidia driver ([fc1dd35](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/fc1dd35cef59665c385c72c77fb1ec54b8c2e2de))
* pin ansible galaxy dependencies ([8529ea7](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/8529ea71d8b2fe8bc668d67524ff3846a677a3e8))


### Bug Fixes

* disable bonding and bridging on debian 11 ([fbdc197](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/fbdc19743bc09d0d7c5d4eae0b58d132dc240df8))
* **docker_setup:** fix docker installation ([428f3b4](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/428f3b48044e15a25d363e5875b5a02222229a7f))
* ensure cron is installed before setting cronjobs ([3ff5713](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/3ff5713343059449f3ae1da9cdb644d9b1c861e0))


### Tests

* change collection type ([847fbee](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/847fbee01a292e4347787e1d42a38f3e3c97d380))
* use vagrant boxes that are directly provided by the distribution ([48412e0](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/48412e0f23f561d0e838c3934f0423fbb4c6975e))

## [2.8.1](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v2.8.0...v2.8.1) (2024-02-23)


### Continuous Integration

* add job for deploying ansible collection ([39a54ec](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/39a54ec2c1b27d64f5ffc697984531431c449537))

## [2.8.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v2.7.0...v2.8.0) (2024-02-23)


### Features

* **linux_network:** add bonding and bridging ([76230af](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/76230af19840a264d240ecd3cec30d9498629773))
* **linux_network:** make name of interface optional ([8c3b613](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/8c3b6136b1483e569205de37645fbb20d9f8d0ea))
* **pve_iommu:** blacklist gpu drivers ([7a91728](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/7a91728be37333b913db9fa2a6a7323fcabd94a7))
* **pve_vm:** add new role to configure pve vms ([14826ba](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/14826ba8c33577fa7d285c1915b4ff1337997441))

## [2.7.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v2.6.0...v2.7.0) (2024-02-01)


### Features

* add enviroment for tests ([620d8b1](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/620d8b18be7bc6b4ea7aa1ddc8f48c8941e2f770))
* add new roles for configuring pve ([7a856dc](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/7a856dc3efc1f329641c1e6c7b9102dd35416fb8))
* add suggestions from ansible-lint ([28d3ec0](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/28d3ec01ce0d6c3686f9e0c93881609c4e6bab73))
* add yq-sort tool ([cedf3b4](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/cedf3b40033cc7a8da24ecaae11759c8a7e56ca4))
* format ansible files with yq-sort ([4399aff](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/4399aff6acb394c15c0f17cec64f3112aa316c10))
* **k3s:** add new role to for k3s ([eb1cac9](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/eb1cac938705468f491f95358675177a1f5d5764))
* update roles according to best practices ([c19f727](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/c19f7274f8c9b74c2a82d4173be46b5ee4d5f228))


### Bug Fixes

* **gitlab_projects:** disable configuration for protected branches and tags ([902940f](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/902940fe8bdb30f397c06f4299f46f572ed4b3ee))

## [2.6.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v2.5.0...v2.6.0) (2023-07-25)


### Features

* add dependencies from roles to galaxy.yml ([3118c61](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/3118c61d3f6600c5f9b24c9f75c3207575f8f090))
* add initial support for debian 12 (bookworm) ([0af2c3f](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/0af2c3fc51991e863c992bd54b93b4164758a64d))
* **docker_compose:** add backup script to role ([3de986b](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/3de986ba2e55c3c63625016229611c2167c86abe))
* **docker_compose:** set src and dest path as optional ([dc5deb0](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/dc5deb00d99be74c534cf68f23a3d84d0f6176a4))
* **linux_packages:** add new role to install basic packages ([15c1d73](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/15c1d733c3e0ab72e2f6ddaa789cd083b7b95a6e))


### Bug Fixes

* add ansible.utils to dependencies ([1532e7f](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/1532e7f01928fcb5ad7525c0ad094b506a19f60e))
* **docker_setup:** do not update docker packages ([ada4645](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/ada464574f3067a143d44cd369fc830405ca0060))
* remove ansible.builtin from dependencies ([8ccfae5](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/8ccfae50bd776d98a36af4b27cb3ff08f7203422))
* use fqdn for include_tasks ([b1622e8](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/b1622e8866a535af4bc569d1df0b1a31f8159f92))
* use fqdn for meta ([105960e](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/105960e306259412dda230b47f784daa0ba5aa95))
* use ipaddr module from ansible.utils ([d5749e4](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/d5749e4d1a2f3f33d99eadf542aab00db7e9ae1a))


### Documentation

* update dc-backup help text ([bfbc2cd](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/bfbc2cd4110ee7bc77c3deb36bbe4fe14e864dc3))

## [2.5.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v2.4.0...v2.5.0) (2023-06-14)


### Features

* **docker_setup:** add dc-backup script and yq installation ([b2fa930](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/b2fa93043b7751d87ad6c2603ec99eab2dcb0ea9))

## [2.4.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v2.3.0...v2.4.0) (2023-05-04)


### Features

* **linux_root:** ignore python warnings on centos 7 ([17e7dd9](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/17e7dd973b622d22352d9033cc958178f4bcc199))
* **tailscale:** add new role for tailscale ([a6087f7](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/a6087f77bd1f1c6c685e5a09e0515f3766e2e9a1))
* update gitlab-ci-templates to v3.1.1 ([84414f2](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/84414f256002fc54a82baf1fc3bcb61738659591))


### Bug Fixes

* **docker_setup:** enable docker service on centos 7 ([d02f366](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/d02f3660ca587bfcf116b7cac9ecde3fd419c25e))

## [2.3.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v2.2.0...v2.3.0) (2023-02-19)


### Features

* **docker_compose:** allow multiple executions of extra files ([c67110c](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/c67110c4d52a5529098216c13c295bbc79ea8436))


### Bug Fixes

* **docker_compose:** always remove temporary directory even when task fails ([582527a](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/582527a475e9ad3ef7cee88360bc0fb475973f9d))
* **docker_compose:** always use Python 3 when configuring services ([92d17ce](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/92d17ce9b34794688fe05ba7281e387987a8b893))
* **docker_setup:** fix pip3 package insall when using Python 2 on CentOS 7 ([6fa1173](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/6fa1173314cd93b2e4d8ddc57e08510a06ea858f))

## [2.2.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v2.1.0...v2.2.0) (2023-02-08)


### Features

* add role pve_setup ([d273ec9](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/d273ec9c47cb4e9e5748b654e92aeabfd9cfdc93))
* **docker_compose:** add role docker_compose ([59a92d8](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/59a92d8589c9847ea40320ff6f1658a7c44633fd))
* **docker_setup:** add role docker_setup ([bc7e2dd](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/bc7e2dddb2d335c7449680909775a62ec00de454))


### Documentation

* **hetzner_hcloud:** mark primary ip aliases as required when used by another role ([8e3b0a5](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/8e3b0a54438ae1449a0686043ab20c5e7929142b))

## [2.1.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v2.0.0...v2.1.0) (2023-02-04)


### Features

* add dependencies to galaxy.yml ([562daa5](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/562daa56504c6b63b09115a6e968fac372c50a46))

## [2.0.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v1.2.0...v2.0.0) (2023-02-04)


### ⚠ BREAKING CHANGES

* rename repository to ansible-collection-general

### Features

* add roles for linux server configuration ([c489013](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/c489013ccc41b8b28f5fe37d9c47e28da4b6376f))
* rename repository to ansible-collection-general ([ea3de65](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/ea3de65c5af368508d9ef57f00be23222334cb40))

## [1.2.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v1.1.0...v1.2.0) (2022-12-09)


### Features

* **hetzner_hcloud:** add aliases to primary ip, can be read by other roles ([dc17209](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/dc1720929f33580075d265cb50af3307ad132cfb))
* **hetzner_hcloud:** add labels to server ([4ce7ffe](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/4ce7ffedcb8c696a9ffcec9987622a99287ccd1b))
* **hetzner_hcloud:** refresh inventory at the end ([bc99a5a](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/bc99a5a4bc5069a749ae19045cd8a5c0c82cfb75))
* **hetzner_hcloud:** set server name to fqdn and configure reverse dns entry ([5255bf8](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/5255bf8ac4efaec196e8a7c3b7661435ec90f462))
* **inwx_dns:** add default domain ([ed94a0c](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/ed94a0c0f87db1983730d6df0c9e0ad579e14581))
* **inwx_dns:** add dns configuration for hetzner hcloud ([4dcdab5](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/4dcdab5e86c65d796905719a1c66019319d58f84))
* **inwx_dns:** change default ttl from 3600 to 300 ([4cc4d07](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/4cc4d072eed2db38aa07c0a7516288a5bca0c6f6))


### Bug Fixes

* **inwx_dns:** patch inwx.collection.dns v1.3.0 to fix records not deletable ([4e8df9e](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/4e8df9ea933100d2610cdbe89d2bdf78d13d1b9f))


### Documentation

* fix variable types in inwx_dns docs ([beabdf0](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/beabdf0b6bbc44538a26ec0423210bb2f89838cc))

## [1.1.0](https://gitlab.com/omnitoolkit/ansible-collection-general/compare/v1.0.0...v1.1.0) (2022-12-03)


### Features

* add role hetzner_hcloud ([01a71fb](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/01a71fb0ada52293653fe8a6f641d145bf3d577e))
* add role inwx_dns ([aaa3801](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/aaa3801a6abcc66a070a88e9204a983b0846225a))
* set minimum ansible version to 2.13 ([dae73ae](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/dae73ae04ec7dfa719f939f427722400d4d3df6a))

## 1.0.0 (2022-11-26)


### Features

* add collection structure ([5cb8376](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/5cb8376e5def0fed647d9caac1db533a7a584dc7))
* add role gitlab_groups ([055954f](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/055954fe23ddf97cb2941283f521c8bdd301ce1d))
* add role gitlab_projects ([fb97475](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/fb974757c476d0adcf8afe2a3ffefe04bc9ce8e2))


### Continuous Integration

* add ci job for release ([e4fd2da](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/e4fd2da6b987e5de654f23b9e997f83e7909a5e4))


### Documentation

* remove variable name from roles table ([dc74282](https://gitlab.com/omnitoolkit/ansible-collection-general/commit/dc742824064007d34e4bfa3f1b6447b1b244dded))
