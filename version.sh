#!/usr/bin/env sh

set -eu

sed -i "s/^version:.*$/version: ${1}/g" ./galaxy.yml

sed -i "s/^    version:.*$/    version: ${1}/g" ./tests/ansible/requirements.yml
