# Ansible collection 'omnitoolkit.general'

This collection contains roles for various use cases.

## Roles

| Role (click for documentation) | Description | Required variables | Additional info | OS |
| --- | --- | --- | --- | --- |
| [omnitoolkit.general.docker_compose](roles/docker_compose/README.md) | This role configures docker-compose services on the system. | None. | None. | <ul><li>Debian bookworm</li><li>EL 7</li></ul> |
| [omnitoolkit.general.docker_setup](roles/docker_setup/README.md) | This role installs Docker on the system. | None. | None. | <ul><li>Debian bookworm</li><li>EL 7</li></ul> |
| [omnitoolkit.general.gitlab_groups](roles/gitlab_groups/README.md) | This role configures settings, members and variables of GitLab groups. | See docs. | None. | <ul><li>GenericLinux all</li></ul> |
| [omnitoolkit.general.gitlab_projects](roles/gitlab_projects/README.md) | This role configures settings, protected branches and tags of GitLab projects. | See docs. | None. | <ul><li>GenericLinux all</li></ul> |
| [omnitoolkit.general.hetzner_hcloud](roles/hetzner_hcloud/README.md) | This role configures hetzner hcloud. For dynamic inventory, copy the file `hcloud.yml` from the files directory to your inventory. | See docs. | Multiple caveats. | <ul><li>GenericLinux all</li></ul> |
| [omnitoolkit.general.inwx_dns](roles/inwx_dns/README.md) | This role configures inwx dns. | See docs. | None. | <ul><li>GenericLinux all</li></ul> |
| [omnitoolkit.general.k3s_setup](roles/k3s_setup/README.md) | This role installs k3s on the system. | None. | None. | <ul><li>Debian bookworm</li></ul> |
| [omnitoolkit.general.linux_firewall](roles/linux_firewall/README.md) | This role configures the firewall via iptables. | None. | By default, the role will allow all traffic through the firewall. It is strongly recommended to set the variable `linux_firewall__policy_input` to `DROP` and define the variable `linux_firewall__rules__*` to allow SSH access. | <ul><li>Debian bookworm</li><li>EL 7</li></ul> |
| [omnitoolkit.general.linux_handbrake](roles/linux_handbrake/README.md) | This role shows a prompt and only continues after confirmation. | None. | By default, the role will have no effect. It's goal is to protect specific hosts or groups from accidental provisioning. | <ul><li>Debian bookworm</li><li>EL 7</li></ul> |
| [omnitoolkit.general.linux_locale](roles/linux_locale/README.md) | This role configures keyboard mappings and the system language. | None. | None. | <ul><li>Debian bookworm</li><li>EL 7</li></ul> |
| [omnitoolkit.general.linux_motd](roles/linux_motd/README.md) | This role configures the message of the day. | None. | None. | <ul><li>Debian bookworm</li><li>EL 7</li></ul> |
| [omnitoolkit.general.linux_network](roles/linux_network/README.md) | This role configures network interfaces, DNS, hostname, hosts file, static routing and policy-based routing. | None. | By default, the role will guess the configuration for the default interface, ignoring all other interfaces. It is strongly recommended to define the variable `linux_network__interfaces` for each host. | <ul><li>Debian bookworm</li><li>EL 7</li></ul> |
| [omnitoolkit.general.linux_packages](roles/linux_packages/README.md) | This role installs basic packages onto the system. | None. | Firewall-, locale-, network-, time- and vm-related packages are installed in their respective roles. | <ul><li>Debian bookworm</li><li>EL 7</li></ul> |
| [omnitoolkit.general.linux_root](roles/linux_root/README.md) | This role configures the root password and the files `.ssh/authorized_keys`, `.bashrc` and `.vimrc` in the root folder. | None. | By default, the role will skip configuration for root password and `.ssh/authorized_keys` file. It is strongly recommended to define the variables `linux_root__password` and `linux_root__authorized_keys__*`. | <ul><li>Debian bookworm</li><li>EL 7</li></ul> |
| [omnitoolkit.general.linux_sshd](roles/linux_sshd/README.md) | This role configures the SSH server settings. | None. | By default, the role will allow SSH login via password. It is strongly recommended to define the variable `linux_sshd__config__*` (see the example there) and add your ssh key to the `root` role variable `linux_root__authorized_keys__*`. | <ul><li>Debian bookworm</li><li>EL 7</li></ul> |
| [omnitoolkit.general.linux_swap](roles/linux_swap/README.md) | This role configures the swappiness of the system. | None. | None. | <ul><li>Debian bookworm</li><li>EL 7</li></ul> |
| [omnitoolkit.general.linux_time](roles/linux_time/README.md) | This role configures the time zone and the time servers. | None. | None. | <ul><li>Debian bookworm</li><li>EL 7</li></ul> |
| [omnitoolkit.general.linux_update](roles/linux_update/README.md) | This role updates the installed packages and reboots the server if necessary. | None. | None. | <ul><li>Debian bookworm</li><li>EL 7</li></ul> |
| [omnitoolkit.general.nvidia_driver](roles/nvidia_driver/README.md) | This role configures the nvidia driver. | None. | None. | <ul><li>Debian bookworm</li></ul> |
| [omnitoolkit.general.pve_iommu](roles/pve_iommu/README.md) | This role configures IOMMU of a Proxmox VE system. | None. | None. | <ul><li>Debian bookworm</li></ul> |
| [omnitoolkit.general.pve_notice](roles/pve_notice/README.md) | This role configures the subscription notice of a Proxmox VE system. | None. | None. | <ul><li>Debian bookworm</li></ul> |
| [omnitoolkit.general.pve_repos](roles/pve_repos/README.md) | This role configures the repositories of a Proxmox VE system. | None. | None. | <ul><li>Debian bookworm</li></ul> |
| [omnitoolkit.general.pve_setup](roles/pve_setup/README.md) | This role configures the setup of a Proxmox VE system. | None. | None. | <ul><li>Debian bookworm</li></ul> |
| [omnitoolkit.general.pve_vm](roles/pve_vm/README.md) | This role configures VMs of a Proxmox VE system. | None. | None. | <ul><li>GenericLinux all</li></ul> |
| [omnitoolkit.general.tailscale_setup](roles/tailscale_setup/README.md) | This role installs Tailscale on the system. | None. | None. | <ul><li>Debian bookworm</li></ul> |


## Dependencies

- ansible.posix: 1.5.4 # https://github.com/ansible-collections/ansible.posix/releases
- ansible.utils: 3.1.0 # https://github.com/ansible-collections/ansible.utils/releases
- community.docker: 3.8.0 # https://github.com/ansible-collections/community.docker/releases
- community.general: 8.4.0 # https://github.com/ansible-collections/community.general/releases
- hetzner.hcloud: 1.9.0 # https://github.com/ansible-collections/hetzner.hcloud/releases
- inwx.collection: 1.3.1 # https://github.com/inwx/ansible-collection/releases

## License

- Apache-2.0

## Author

omnitoolkit contributors
