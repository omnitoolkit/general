# Ansible role 'linux_locale'

**Description:** This role configures keyboard mappings and the system language.

**Required variables:** None.

**Additional info:** None.

## Table of content

- [Default Variables](#default-variables)
  - [linux_locale__keymap](#linux_locale__keymap)
  - [linux_locale__language](#linux_locale__language)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### linux_locale__keymap

String, optional.

Keymap. List all available values with `localectl list-keymaps` (will not work on all systems).

#### Default value

```YAML
linux_locale__keymap: de-latin1
```

### linux_locale__language

String, optional.

Language. Also determines date and time formatting, character display, currency display and codepage selection. List all available values with `localectl list-locales` (note that "utf8" needs to be written as "UTF-8" here) or `cat /usr/share/i18n/SUPPORTED`.

#### Default value

```YAML
linux_locale__language: en_US.UTF-8
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
