# @var linux_firewall__flush_forward:description: >
# Boolean, optional.
#
# Whether to flush the chain `FORWARD` when starting the firewall.
#
# To prevent conflicts with docker, this is set to `false` by default.
# @end
linux_firewall__flush_forward: false
#
# @var linux_firewall__flush_input:description: >
# Boolean, optional.
#
# Whether to flush the chain `INPUT` when starting the firewall.
# @end
linux_firewall__flush_input: true
#
# @var linux_firewall__flush_output:description: >
# Boolean, optional.
#
# Whether to flush the chain `OUTPUT` when starting the firewall.
# @end
linux_firewall__flush_output: true
#
# @var linux_firewall__policy_forward:description: >
# String, optional.
#
# Policy for chain `FORWARD` in iptables syntax.
# @end
linux_firewall__policy_forward: DROP
#
# @var linux_firewall__policy_input:description: >
# String, optional.
#
# Policy for chain `INPUT` in iptables syntax.
# @end
linux_firewall__policy_input: ACCEPT
#
# @var linux_firewall__policy_output:description: >
# String, optional.
#
# Policy for chain `OUTPUT` in iptables syntax.
# @end
linux_firewall__policy_output: ACCEPT
#
# @var linux_firewall__rules__*:description: >
# List, optional.
#
# Contains dicts, each containing a firewall rule in iptables syntax.
#
# For each entry in `linux_network__interfaces`, a chain is created
# and packages going to this interface are redirected to the created chain. The name will
# be the interface's `name`, so rules can be added by using this name in `chain`.
#
# Please note the example. Most variables are optional and most values can be negated by
# prefixing `!`. Prepend a number to the name to sort the entries. For ports, use `,` to
# define individual ports and `:` to define port ranges.
#
# All variables starting with `linux_firewall__rules__` are merged
# and sorted by `name` on runtime for each host. `*` is a placeholder for any string,
# however it is recommended to use a meaningful and unique name.
#
# If no variables are defined (which is the default), only the default rules from below are applied.
# @end
# @var linux_firewall__rules__*: >
# # undefined
# @end
# @var linux_firewall__rules__*:example: >
# linux_firewall__rules__appserver:
#   - name: 01 rule with all possible variables
#     chain: INPUT
#     iface_in: lo
#     iface_out: "!eth0"
#     ip_dest: "!192.168.0.0/16"
#     ip_src: 192.168.0.0/16
#     jump: REJECT --reject-with tcp-reset
#     match:
#       - conntrack --ctstate RELATED,ESTABLISHED
#       - recent --name ICMP --set
#     ports_dest: 22,80,443
#     ports_src: 8080:8090
#     protocol: "!tcp"
#
#   - name: 02 rule with minimal variables
#     chain: INPUT
#     jump: ACCEPT
# @end
#
# @var linux_firewall__rules_default:description: >
# List, optional.
#
# Default firewall rules in iptables syntax. These will be applied before the self-defined entries.
#
# To disable them, overwrite the variable as shown in the example.
# @end
# @var linux_firewall__rules_default:example: >
# # disable default rules
# linux_firewall__rules_default: []
# @end
linux_firewall__rules_default:
  - name: 01 Don't attempt to firewall internal traffic on the loopback device
    chain: INPUT
    iface_in: lo
    jump: ACCEPT
  #
  - name: 02 Continue connections that are already established or related to an established connection
    chain: INPUT
    jump: ACCEPT
    match: conntrack --ctstate RELATED,ESTABLISHED
  #
  - name: 03 Drop non-conforming packets, such as malformed headers, etc.
    chain: INPUT
    jump: DROP
    match: conntrack --ctstate INVALID
  #
  - name: 04 Block remote packets claiming to be from a loopback address
    chain: INPUT
    iface_in: "!lo"
    ip_src: 127.0.0.0/8
    jump: DROP
  #
  - name: 05 Drop all packets that are going to broadcast, multicast or anycast address
    chain: INPUT
    jump: DROP
    match: addrtype --dst-type BROADCAST
  #
  - name: 06 Drop all packets that are going to broadcast, multicast or anycast address
    chain: INPUT
    jump: DROP
    match: addrtype --dst-type MULTICAST
  #
  - name: 07 Drop all packets that are going to broadcast, multicast or anycast address
    chain: INPUT
    jump: DROP
    match: addrtype --dst-type ANYCAST
  #
  - name: 08 Drop all packets that are going to broadcast, multicast or anycast address
    chain: INPUT
    ip_dest: 224.0.0.0/4
    jump: DROP
  #
  - name: 09 Permit useful IMCP packet types for IPv4
    chain: INPUT
    jump: ACCEPT
    protocol: icmp --icmp-type 0
  #
  - name: 10 Permit useful IMCP packet types for IPv4
    chain: INPUT
    jump: ACCEPT
    protocol: icmp --icmp-type 3
  #
  - name: 11 Permit useful IMCP packet types for IPv4
    chain: INPUT
    jump: ACCEPT
    protocol: icmp --icmp-type 11
  #
  - name: 12 Permit IMCP echo requests (ping) and use recent module for preventing ping flooding
    chain: INPUT
    match: recent --name ICMP --set
    protocol: icmp --icmp-type 8
  #
  - name: 13 Permit IMCP echo requests (ping) and use recent module for preventing ping flooding
    chain: INPUT
    jump: DROP
    match: recent --name ICMP --update --seconds 1 --hitcount 10 --rttl
    protocol: icmp --icmp-type 8
  #
  - name: 14 Permit IMCP echo requests (ping) and use recent module for preventing ping flooding
    chain: INPUT
    jump: ACCEPT
    protocol: icmp --icmp-type 8
