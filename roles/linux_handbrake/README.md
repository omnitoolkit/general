# Ansible role 'linux_handbrake'

**Description:** This role shows a prompt and only continues after confirmation.

**Required variables:** None.

**Additional info:** By default, the role will have no effect. It's goal is to protect specific hosts or groups from accidental provisioning.

## Table of content

- [Default Variables](#default-variables)
  - [linux_handbrake__set](#linux_handbrake__set)
- [Discovered Tags](#discovered-tags)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### linux_handbrake__set

Boolean, optional.

Whether to pause the play and show a prompt.

#### Default value

```YAML
linux_handbrake__set: false
```

#### Example usage

```YAML
linux_handbrake__set: true
```

## Discovered Tags

**_always_**\
&emsp;Allows the prompt to show even if a specific tag is called.


## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
