# @var hetzner_hcloud__api_token:description: >
# String, required.
#
# Contains the API token, needs read and write access.
# @end
hetzner_hcloud__api_token: ""
#
# @var hetzner_hcloud__domain:description: >
# String, optional.
#
# Contains a domain name, will be appended to all server names.
# @end
hetzner_hcloud__domain: ""
#
# @var hetzner_hcloud__firewall_default_rules:description: >
# List, optional.
#
# Contains dicts with default value for firewall rules.
# @end
hetzner_hcloud__firewall_default_rules:
  - description: allow icmp in
    direction: in
    protocol: icmp
    source_ips:
      - 0.0.0.0/0
      - ::/0
  - description: allow ssh in
    direction: in
    port: 22
    protocol: tcp
    source_ips:
      - 0.0.0.0/0
      - ::/0
#
# @var hetzner_hcloud__firewall_list:description: >
# List, optional.
#
# Contains dicts, each containing a firewall configuration.
#
# Please note the the example.
# @end
# @var hetzner_hcloud__firewall_list:example: >
# hetzner_hcloud__firewall_list:
#   - name: my-firewall       # optional, defaults to <cluster name>-firewall-<entry number>
#     state: present          # optional, defaults to cluster state
#     rules:                  # optional, has default
#       - description: allow ssh in
#         direction: in
#         port: 22
#         protocol: tcp
#         source_ips:
#           - 0.0.0.0/0
#           - ::/0
# @end
hetzner_hcloud__firewall_list: []
#
# @var hetzner_hcloud__name:description: >
# String, required.
#
# Contains the name of the cluster, will be used in resource names.
# @end
hetzner_hcloud__name: ""
#
# @var hetzner_hcloud__network_default_ip_range:description: >
# String, optional.
#
# Contains the default value for network ip range.
# @end
hetzner_hcloud__network_default_ip_range: 10.0.0.0/24
#
# @var hetzner_hcloud__network_default_type:description: >
# String, optional.
#
# Contains the default value for network type.
# @end
hetzner_hcloud__network_default_type: cloud
#
# @var hetzner_hcloud__network_default_zone:description: >
# String, optional.
#
# Contains the default value for network zone.
# @end
hetzner_hcloud__network_default_zone: eu-central
#
# @var hetzner_hcloud__network_list:description: >
# List, optional.
#
# Contains dicts, each containing a network configuration.
#
# Please note the the example.
# @end
# @var hetzner_hcloud__network_list:example: >
# hetzner_hcloud__network_list:
#   - name: my-network        # optional, defaults to <cluster name>-network-<entry number>
#     state: present          # optional, defaults to cluster state
#     ip_range: 10.0.0.0/24   # optional, has default, cannot be changed after creation
#     network_zone: eu-central # optional, has default, cannot be changed after creation
#     type: cloud             # optional, has default, cannot be changed after creation
# @end
hetzner_hcloud__network_list: []
#
# @var hetzner_hcloud__placement_group_default_type:description: >
# String, optional.
#
# Contains the default value for placement group type.
# @end
hetzner_hcloud__placement_group_default_type: spread
#
# @var hetzner_hcloud__placement_group_list:description: >
# List, optional.
#
# Contains dicts, each containing a placement group configuration.
#
# Please note the the example.
# @end
# @var hetzner_hcloud__placement_group_list:example: >
# hetzner_hcloud__placement_group_list:
#   - name: my-pl-group       # optional, defaults to <cluster name>-placement-group-<entry number>
#     state: present          # optional, defaults to cluster state
#     type: spread            # optional, has default, cannot be changed after creation
# @end
hetzner_hcloud__placement_group_list: []
#
# @var hetzner_hcloud__primary_ip_default_datacenter:description: >
# String, optional.
#
# Contains the default value for primary ip datacenter.
# @end
hetzner_hcloud__primary_ip_default_datacenter: fsn1-dc14
#
# @var hetzner_hcloud__primary_ip_default_type:description: >
# String, optional.
#
# Contains the default value for primary ip type.
# @end
hetzner_hcloud__primary_ip_default_type: ipv4
#
# @var hetzner_hcloud__primary_ip_list:description: >
# List, optional.
#
# Contains dicts, each containing a primary ip configuration.
#
# Please note the the example.
# @end
# @var hetzner_hcloud__primary_ip_list:example: >
# hetzner_hcloud__primary_ip_list:
#   - name: my-primary-ip     # optional, defaults to <cluster name>-primary-ip-<entry number>
#     state: present          # optional, defaults to cluster state
#     aliases:                # optional, no default, does nothing on its own, required when used by another role, e.g. inwx_dns
#       - record: ip          # required, will be prepended to domain
#         state: present      # optional, defaults to cluster state
#     datacenter: fsn1-dc14   # optional, has default, cannot be changed after creation
#     type: ipv4              # optional, has default, cannot be changed after creation
# @end
hetzner_hcloud__primary_ip_list: []
#
# @var hetzner_hcloud__server_default_backups:description: >
# Boolean, optional.
#
# Contains the default value for server backups.
# @end
hetzner_hcloud__server_default_backups: true
#
# @var hetzner_hcloud__server_default_datacenter:description: >
# String, optional.
#
# Contains the default value for server datacenter.
# @end
hetzner_hcloud__server_default_datacenter: fsn1-dc14
#
# @var hetzner_hcloud__server_default_force:description: >
# Boolean, optional.
#
# Contains the default value for server force.
# @end
hetzner_hcloud__server_default_force: true
#
# @var hetzner_hcloud__server_default_image:description: >
# String, optional.
#
# Contains the default value for server image.
# @end
hetzner_hcloud__server_default_image: debian-11
#
# @var hetzner_hcloud__server_default_labels:description: >
# List, optional.
#
# Contains dicts, each containing key-value pairs. This will set a label with the cluster name as key and no value.
# @end
hetzner_hcloud__server_default_labels:
  - key: "{{ hetzner_hcloud__name }}"
    value: ""
  - key: "{{ hetzner_hcloud__name }}-servers"
    value: ""
#
# @var hetzner_hcloud__server_default_location:description: >
# String, optional.
#
# Contains the default value for server location.
# @end
hetzner_hcloud__server_default_location: fsn1
#
# @var hetzner_hcloud__server_default_type:description: >
# String, optional.
#
# Contains the default value for server type.
# @end
hetzner_hcloud__server_default_type: cx11
#
# @var hetzner_hcloud__server_list:description: >
# List, optional.
#
# Contains dicts, each containing a server configuration.
#
# Please note the the example.
# @end
# @var hetzner_hcloud__server_list:example: >
# hetzner_hcloud__server_list:
#   - name: my-server         # optional, defaults to <cluster name>-server-<entry number>
#     state: present          # optional, defaults to cluster state
#     backups: true           # optional, has default
#     datacenter: fsn1-dc14   # optional, has default, cannot be changed after creation
#     image: debian-11        # optional, has default
#     labels:                 # optional, will be merged with default labels
#       - key: mykey          # required, no default
#         value: myvalue      # optional, no default
#     location: fsn1          # optional, defaults to datacenter, cannot be changed after creation
#     map_firewalls: 1        # optional, no default, maps to entry number of firewalls, cannot be deleted via ansible
#     map_ipv4: 1             # optional, no default, maps to entry number of primary ips
#     map_ipv6: 1             # optional, no default, maps to entry number of primary ips
#     map_placement_group: 1  # optional, no default, maps to entry number of placement groups
#     map_private_networks: 1 # optional, no default, maps to entry number of networks
#     map_ssh_keys: 1         # optional, no default, maps to entry number of ssh keys
#     map_volumes: 1          # optional, no default, maps to entry number of volumes
#     server_type: cx11       # optional, has default, cannot be changed after creation
# @end
hetzner_hcloud__server_list: []
#
# @var hetzner_hcloud__ssh_key_list:description: >
# List, optional.
#
# Contains dicts, each containing a ssh key configuration.
#
# Please note the the example.
# @end
# @var hetzner_hcloud__ssh_key_list:example: >
# hetzner_hcloud__ssh_key_list:
#   - name: my-ssh-key        # optional, defaults to <cluster name>-ssh-key-<entry number>
#     state: present          # optional, defaults to cluster state
#     public_key: ssh-rsa..   # required, no default
# @end
hetzner_hcloud__ssh_key_list: []
#
# @var hetzner_hcloud__state:description: >
# String, optional.
#
# Contains the state of the cluster, can be `present` or `absent`.
# @end
hetzner_hcloud__state: present
#
# @var hetzner_hcloud__volume_default_automount:description: >
# Boolean, optional.
#
# Contains the default value for volume automount.
# @end
hetzner_hcloud__volume_default_automount: true
#
# @var hetzner_hcloud__volume_default_format:description: >
# String, optional.
#
# Contains the default value for volume format.
# @end
hetzner_hcloud__volume_default_format: ext4
#
# @var hetzner_hcloud__volume_default_location:description: >
# String, optional.
#
# Contains the default value for volume location.
# @end
hetzner_hcloud__volume_default_location: fsn1
#
# @var hetzner_hcloud__volume_default_size:description: >
# Integer, optional.
#
# Contains the default value for volume size.
# @end
hetzner_hcloud__volume_default_size: 10
#
# @var hetzner_hcloud__volume_list:description: >
# List, optional.
#
# Contains dicts, each containing a volume configuration.
#
# Please note the the example.
# @end
# @var hetzner_hcloud__volume_list:example: >
# hetzner_hcloud__volume_list:
#   - name: my-volume         # optional, defaults to <cluster name>-volume-<entry number>
#     state: present          # optional, defaults to cluster state
#     automount: true         # optional, has default
#     format: ext4            # optional, has default, cannot be changed after creation
#     location: fsn1          # optional, has default, cannot be changed after creation
#     map_server: 1           # optional, no default, maps to entry number of servers
#     size: 10                # optional, has default
# @end
hetzner_hcloud__volume_list: []
