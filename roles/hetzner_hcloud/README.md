# Ansible role 'hetzner_hcloud'

**Description:** This role configures hetzner hcloud. For dynamic inventory, copy the file `hcloud.yml` from the files directory to your inventory.

**Required variables:** See docs.

**Additional info:** Multiple caveats.

- When setting the state of a resource to `absent`, the mapping at the server must be manually deleted since the check is not yet implemented.

- Currently, multiple maps of firewalls, networks, ssh keys or volumes to a server are not yet implemented.

- When updating placement group, primary ip or server type on a server, the server is automatically rebooted.

- Updating datacenter, image, location, or ssh-keys on a server has no effect and does not return any feedback on this.

- When updating placement group, primary ip or server type on a server, only one resource can be changed at a time, multiple changes will lead to errors. This is a bug in the collection `hetzner.hcloud` (v1.9.0).

- The firewall entry on a server does not show as changed in ansible check mode.

- The firewall entry on a server cannot be removed via ansible. This is a bug in the collection `hetzner.hcloud` (v1.9.0). It must be done via ui.

- The volume entry on a server cannot be updated via ansible. This is a bug in the collection `hetzner.hcloud` (v1.9.0). It is mitigated by explicitly attaching to server on volume configuration.

## Table of content

- [Default Variables](#default-variables)
  - [hetzner_hcloud__api_token](#hetzner_hcloud__api_token)
  - [hetzner_hcloud__domain](#hetzner_hcloud__domain)
  - [hetzner_hcloud__firewall_default_rules](#hetzner_hcloud__firewall_default_rules)
  - [hetzner_hcloud__firewall_list](#hetzner_hcloud__firewall_list)
  - [hetzner_hcloud__name](#hetzner_hcloud__name)
  - [hetzner_hcloud__network_default_ip_range](#hetzner_hcloud__network_default_ip_range)
  - [hetzner_hcloud__network_default_type](#hetzner_hcloud__network_default_type)
  - [hetzner_hcloud__network_default_zone](#hetzner_hcloud__network_default_zone)
  - [hetzner_hcloud__network_list](#hetzner_hcloud__network_list)
  - [hetzner_hcloud__placement_group_default_type](#hetzner_hcloud__placement_group_default_type)
  - [hetzner_hcloud__placement_group_list](#hetzner_hcloud__placement_group_list)
  - [hetzner_hcloud__primary_ip_default_datacenter](#hetzner_hcloud__primary_ip_default_datacenter)
  - [hetzner_hcloud__primary_ip_default_type](#hetzner_hcloud__primary_ip_default_type)
  - [hetzner_hcloud__primary_ip_list](#hetzner_hcloud__primary_ip_list)
  - [hetzner_hcloud__server_default_backups](#hetzner_hcloud__server_default_backups)
  - [hetzner_hcloud__server_default_datacenter](#hetzner_hcloud__server_default_datacenter)
  - [hetzner_hcloud__server_default_force](#hetzner_hcloud__server_default_force)
  - [hetzner_hcloud__server_default_image](#hetzner_hcloud__server_default_image)
  - [hetzner_hcloud__server_default_labels](#hetzner_hcloud__server_default_labels)
  - [hetzner_hcloud__server_default_location](#hetzner_hcloud__server_default_location)
  - [hetzner_hcloud__server_default_type](#hetzner_hcloud__server_default_type)
  - [hetzner_hcloud__server_list](#hetzner_hcloud__server_list)
  - [hetzner_hcloud__ssh_key_list](#hetzner_hcloud__ssh_key_list)
  - [hetzner_hcloud__state](#hetzner_hcloud__state)
  - [hetzner_hcloud__volume_default_automount](#hetzner_hcloud__volume_default_automount)
  - [hetzner_hcloud__volume_default_format](#hetzner_hcloud__volume_default_format)
  - [hetzner_hcloud__volume_default_location](#hetzner_hcloud__volume_default_location)
  - [hetzner_hcloud__volume_default_size](#hetzner_hcloud__volume_default_size)
  - [hetzner_hcloud__volume_list](#hetzner_hcloud__volume_list)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### hetzner_hcloud__api_token

String, required.

Contains the API token, needs read and write access.

#### Default value

```YAML
hetzner_hcloud__api_token: ''
```

### hetzner_hcloud__domain

String, optional.

Contains a domain name, will be appended to all server names.

#### Default value

```YAML
hetzner_hcloud__domain: ''
```

### hetzner_hcloud__firewall_default_rules

List, optional.

Contains dicts with default value for firewall rules.

#### Default value

```YAML
hetzner_hcloud__firewall_default_rules:
  - description: allow icmp in
    direction: in
    protocol: icmp
    source_ips:
      - 0.0.0.0/0
      - ::/0
  - description: allow ssh in
    direction: in
    port: 22
    protocol: tcp
    source_ips:
      - 0.0.0.0/0
      - ::/0
```

### hetzner_hcloud__firewall_list

List, optional.

Contains dicts, each containing a firewall configuration.

Please note the the example.

#### Default value

```YAML
hetzner_hcloud__firewall_list: []
```

#### Example usage

```YAML
hetzner_hcloud__firewall_list:
  - name: my-firewall       # optional, defaults to <cluster name>-firewall-<entry number>
    state: present          # optional, defaults to cluster state
    rules:                  # optional, has default
      - description: allow ssh in
        direction: in
        port: 22
        protocol: tcp
        source_ips:
          - 0.0.0.0/0
          - ::/0
```

### hetzner_hcloud__name

String, required.

Contains the name of the cluster, will be used in resource names.

#### Default value

```YAML
hetzner_hcloud__name: ''
```

### hetzner_hcloud__network_default_ip_range

String, optional.

Contains the default value for network ip range.

#### Default value

```YAML
hetzner_hcloud__network_default_ip_range: 10.0.0.0/24
```

### hetzner_hcloud__network_default_type

String, optional.

Contains the default value for network type.

#### Default value

```YAML
hetzner_hcloud__network_default_type: cloud
```

### hetzner_hcloud__network_default_zone

String, optional.

Contains the default value for network zone.

#### Default value

```YAML
hetzner_hcloud__network_default_zone: eu-central
```

### hetzner_hcloud__network_list

List, optional.

Contains dicts, each containing a network configuration.

Please note the the example.

#### Default value

```YAML
hetzner_hcloud__network_list: []
```

#### Example usage

```YAML
hetzner_hcloud__network_list:
  - name: my-network        # optional, defaults to <cluster name>-network-<entry number>
    state: present          # optional, defaults to cluster state
    ip_range: 10.0.0.0/24   # optional, has default, cannot be changed after creation
    network_zone: eu-central # optional, has default, cannot be changed after creation
    type: cloud             # optional, has default, cannot be changed after creation
```

### hetzner_hcloud__placement_group_default_type

String, optional.

Contains the default value for placement group type.

#### Default value

```YAML
hetzner_hcloud__placement_group_default_type: spread
```

### hetzner_hcloud__placement_group_list

List, optional.

Contains dicts, each containing a placement group configuration.

Please note the the example.

#### Default value

```YAML
hetzner_hcloud__placement_group_list: []
```

#### Example usage

```YAML
hetzner_hcloud__placement_group_list:
  - name: my-pl-group       # optional, defaults to <cluster name>-placement-group-<entry number>
    state: present          # optional, defaults to cluster state
    type: spread            # optional, has default, cannot be changed after creation
```

### hetzner_hcloud__primary_ip_default_datacenter

String, optional.

Contains the default value for primary ip datacenter.

#### Default value

```YAML
hetzner_hcloud__primary_ip_default_datacenter: fsn1-dc14
```

### hetzner_hcloud__primary_ip_default_type

String, optional.

Contains the default value for primary ip type.

#### Default value

```YAML
hetzner_hcloud__primary_ip_default_type: ipv4
```

### hetzner_hcloud__primary_ip_list

List, optional.

Contains dicts, each containing a primary ip configuration.

Please note the the example.

#### Default value

```YAML
hetzner_hcloud__primary_ip_list: []
```

#### Example usage

```YAML
hetzner_hcloud__primary_ip_list:
  - name: my-primary-ip     # optional, defaults to <cluster name>-primary-ip-<entry number>
    state: present          # optional, defaults to cluster state
    aliases:                # optional, no default, does nothing on its own, required when used by another role, e.g. inwx_dns
      - record: ip          # required, will be prepended to domain
        state: present      # optional, defaults to cluster state
    datacenter: fsn1-dc14   # optional, has default, cannot be changed after creation
    type: ipv4              # optional, has default, cannot be changed after creation
```

### hetzner_hcloud__server_default_backups

Boolean, optional.

Contains the default value for server backups.

#### Default value

```YAML
hetzner_hcloud__server_default_backups: true
```

### hetzner_hcloud__server_default_datacenter

String, optional.

Contains the default value for server datacenter.

#### Default value

```YAML
hetzner_hcloud__server_default_datacenter: fsn1-dc14
```

### hetzner_hcloud__server_default_force

Boolean, optional.

Contains the default value for server force.

#### Default value

```YAML
hetzner_hcloud__server_default_force: true
```

### hetzner_hcloud__server_default_image

String, optional.

Contains the default value for server image.

#### Default value

```YAML
hetzner_hcloud__server_default_image: debian-11
```

### hetzner_hcloud__server_default_labels

List, optional.

Contains dicts, each containing key-value pairs. This will set a label with the cluster name as key and no value.

#### Default value

```YAML
hetzner_hcloud__server_default_labels:
  - key: '{{ hetzner_hcloud__name }}'
    value: ''
  - key: '{{ hetzner_hcloud__name }}-servers'
    value: ''
```

### hetzner_hcloud__server_default_location

String, optional.

Contains the default value for server location.

#### Default value

```YAML
hetzner_hcloud__server_default_location: fsn1
```

### hetzner_hcloud__server_default_type

String, optional.

Contains the default value for server type.

#### Default value

```YAML
hetzner_hcloud__server_default_type: cx11
```

### hetzner_hcloud__server_list

List, optional.

Contains dicts, each containing a server configuration.

Please note the the example.

#### Default value

```YAML
hetzner_hcloud__server_list: []
```

#### Example usage

```YAML
hetzner_hcloud__server_list:
  - name: my-server         # optional, defaults to <cluster name>-server-<entry number>
    state: present          # optional, defaults to cluster state
    backups: true           # optional, has default
    datacenter: fsn1-dc14   # optional, has default, cannot be changed after creation
    image: debian-11        # optional, has default
    labels:                 # optional, will be merged with default labels
      - key: mykey          # required, no default
        value: myvalue      # optional, no default
    location: fsn1          # optional, defaults to datacenter, cannot be changed after creation
    map_firewalls: 1        # optional, no default, maps to entry number of firewalls, cannot be deleted via ansible
    map_ipv4: 1             # optional, no default, maps to entry number of primary ips
    map_ipv6: 1             # optional, no default, maps to entry number of primary ips
    map_placement_group: 1  # optional, no default, maps to entry number of placement groups
    map_private_networks: 1 # optional, no default, maps to entry number of networks
    map_ssh_keys: 1         # optional, no default, maps to entry number of ssh keys
    map_volumes: 1          # optional, no default, maps to entry number of volumes
    server_type: cx11       # optional, has default, cannot be changed after creation
```

### hetzner_hcloud__ssh_key_list

List, optional.

Contains dicts, each containing a ssh key configuration.

Please note the the example.

#### Default value

```YAML
hetzner_hcloud__ssh_key_list: []
```

#### Example usage

```YAML
hetzner_hcloud__ssh_key_list:
  - name: my-ssh-key        # optional, defaults to <cluster name>-ssh-key-<entry number>
    state: present          # optional, defaults to cluster state
    public_key: ssh-rsa..   # required, no default
```

### hetzner_hcloud__state

String, optional.

Contains the state of the cluster, can be `present` or `absent`.

#### Default value

```YAML
hetzner_hcloud__state: present
```

### hetzner_hcloud__volume_default_automount

Boolean, optional.

Contains the default value for volume automount.

#### Default value

```YAML
hetzner_hcloud__volume_default_automount: true
```

### hetzner_hcloud__volume_default_format

String, optional.

Contains the default value for volume format.

#### Default value

```YAML
hetzner_hcloud__volume_default_format: ext4
```

### hetzner_hcloud__volume_default_location

String, optional.

Contains the default value for volume location.

#### Default value

```YAML
hetzner_hcloud__volume_default_location: fsn1
```

### hetzner_hcloud__volume_default_size

Integer, optional.

Contains the default value for volume size.

#### Default value

```YAML
hetzner_hcloud__volume_default_size: 10
```

### hetzner_hcloud__volume_list

List, optional.

Contains dicts, each containing a volume configuration.

Please note the the example.

#### Default value

```YAML
hetzner_hcloud__volume_list: []
```

#### Example usage

```YAML
hetzner_hcloud__volume_list:
  - name: my-volume         # optional, defaults to <cluster name>-volume-<entry number>
    state: present          # optional, defaults to cluster state
    automount: true         # optional, has default
    format: ext4            # optional, has default, cannot be changed after creation
    location: fsn1          # optional, has default, cannot be changed after creation
    map_server: 1           # optional, no default, maps to entry number of servers
    size: 10                # optional, has default
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
