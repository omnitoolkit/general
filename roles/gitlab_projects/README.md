# Ansible role 'gitlab_projects'

**Description:** This role configures settings, protected branches and tags of GitLab projects.

**Required variables:** See docs.

**Additional info:** None.

## Table of content

- [Default Variables](#default-variables)
  - [gitlab_projects__api_token](#gitlab_projects__api_token)
  - [gitlab_projects__api_url](#gitlab_projects__api_url)
  - [gitlab_projects__default_protected_branches](#gitlab_projects__default_protected_branches)
  - [gitlab_projects__default_protected_tags](#gitlab_projects__default_protected_tags)
  - [gitlab_projects__default_settings](#gitlab_projects__default_settings)
  - [gitlab_projects__list](#gitlab_projects__list)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### gitlab_projects__api_token

String, optional.

GitLab API token with role `owner`, can either be a personal, group or project token.

#### Default value

```YAML
gitlab_projects__api_token: ''
```

### gitlab_projects__api_url

String, optional.

GitLab API URL, this should normally be the domain of the GitLab instance.

#### Default value

```YAML
gitlab_projects__api_url: https://gitlab.com
```

### gitlab_projects__default_protected_branches

List, optional.

Contains dicts, each containing a default protected branch. Can be overwritten for specific projects.

#### Default value

```YAML
gitlab_projects__default_protected_branches:
  - name: main
    allow_force_push: false
    merge_access_level: 40
    push_access_level: 40
```

### gitlab_projects__default_protected_tags

List, optional.

Contains dicts, each containing a default protected tag. Can be overwritten for specific projects.

#### Default value

```YAML
gitlab_projects__default_protected_tags:
  - name: v*
    create_access_level: 40
```

### gitlab_projects__default_settings

Dict, optional.

Contains default project settings. Will be merged with the the specific project settings.

#### Default value

```YAML
gitlab_projects__default_settings:
  analytics_access_level: disabled
  auto_devops_enabled: false
  builds_access_level: enabled
  container_expiration_policy_attributes:
    cadence: 1d
    enabled: true
    keep_n: 10
    name_regex: .*
    name_regex_keep: (?:v.+|main)
    older_than: 30d
  container_registry_access_level: enabled
  default_branch: main
  forking_access_level: enabled
  issues_access_level: disabled
  issues_enabled: false
  lfs_enabled: false
  merge_method: ff
  merge_requests_access_level: enabled
  merge_requests_enabled: true
  operations_access_level: disabled
  packages_enabled: false
  pages_access_level: disabled
  public_builds: false
  releases_access_level: enabled
  remove_source_branch_after_merge: true
  repository_access_level: enabled
  request_access_enabled: false
  requirements_access_level: enabled
  security_and_compliance_access_level: disabled
  service_desk_enabled: false
  shared_runners_enabled: true
  snippets_access_level: disabled
  snippets_enabled: false
  squash_option: always
  visibility: public
  wiki_access_level: disabled
  wiki_enabled: false
```

### gitlab_projects__list

List, required.

Contains dicts, each containing a project with namespace (required), settings, protected branches and tags (all optional).

Specific settings will be merged with the default ones above. Specific protected branches and tags will completely override the default ones above.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
gitlab_projects__list:
  - name: exampleproject
    namespace: examplegroup
    settings:
      visibility: private
    protected_branches:
      - name: next
        push_access_level: 40
        merge_access_level: 40
        allow_force_push: false
    protected_tags:
      - name: release-*
        create_access_level: 40
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
