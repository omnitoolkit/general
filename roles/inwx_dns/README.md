# Ansible role 'inwx_dns'

**Description:** This role configures inwx dns.

**Required variables:** See docs.

**Additional info:** None.

## Table of content

- [Default Variables](#default-variables)
  - [inwx_dns__default_domain](#inwx_dns__default_domain)
  - [inwx_dns__default_priority](#inwx_dns__default_priority)
  - [inwx_dns__default_record](#inwx_dns__default_record)
  - [inwx_dns__default_solo](#inwx_dns__default_solo)
  - [inwx_dns__default_ttl](#inwx_dns__default_ttl)
  - [inwx_dns__default_type](#inwx_dns__default_type)
  - [inwx_dns__default_value](#inwx_dns__default_value)
  - [inwx_dns__from_hetzner_hcloud](#inwx_dns__from_hetzner_hcloud)
  - [inwx_dns__list](#inwx_dns__list)
  - [inwx_dns__password](#inwx_dns__password)
  - [inwx_dns__state](#inwx_dns__state)
  - [inwx_dns__username](#inwx_dns__username)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### inwx_dns__default_domain

String, optional.

Contains the default value for domain.

#### Default value

```YAML
inwx_dns__default_domain: ''
```

### inwx_dns__default_priority

Integer, optional.

Contains the default value for priority.

#### Default value

```YAML
inwx_dns__default_priority: 1
```

### inwx_dns__default_record

String, optional.

Contains the default value for record.

#### Default value

```YAML
inwx_dns__default_record: ''
```

### inwx_dns__default_solo

Boolean, optional.

Contains the default value for solo.

#### Default value

```YAML
inwx_dns__default_solo: true
```

### inwx_dns__default_ttl

Integer, optional.

Contains the default value for ttl.

#### Default value

```YAML
inwx_dns__default_ttl: 300
```

### inwx_dns__default_type

String, optional.

Contains the default value for type.

#### Default value

```YAML
inwx_dns__default_type: ''
```

### inwx_dns__default_value

String, optional.

Contains the default value for value.

#### Default value

```YAML
inwx_dns__default_value: ''
```

### inwx_dns__from_hetzner_hcloud

Boolean, optional.

If true, dns entries for servers and for primary ips from hetzner hcloud will be configured. In order to work, the role `hetzner_hcloud` must be executed before this role.

#### Default value

```YAML
inwx_dns__from_hetzner_hcloud: false
```

### inwx_dns__list

List, optional.

Contains dicts, each containing a dns configuration.

Please note the the example.

#### Default value

```YAML
inwx_dns__list: []
```

#### Example usage

```YAML
inwx_dns__list:
  - domain: example.com     # optional, has default (empty)
    priority: 1             # optional, has default, only for MX and SRV
    record: test01          # optional, has default
    solo: true              # optional, has default
    state: present          # optional, has default
    ttl: 3600               # optional, has default
    type: A                 # required, no default
    value: 127.0.0.1        # required, no default
```

### inwx_dns__password

String, required.

Contains the password for inwx.

#### Default value

```YAML
inwx_dns__password: ''
```

### inwx_dns__state

String, optional.

Contains the state of the dns, can be `present` or `absent`.

#### Default value

```YAML
inwx_dns__state: present
```

### inwx_dns__username

String, required.

Contains the username for inwx.

#### Default value

```YAML
inwx_dns__username: ''
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
