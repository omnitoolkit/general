# Ansible role 'linux_packages'

**Description:** This role installs basic packages onto the system.

**Required variables:** None.

**Additional info:** Firewall-, locale-, network-, time- and vm-related packages are installed in their respective roles.

## Table of content

- [Default Variables](#default-variables)
  - [linux_packages__basic_all](#linux_packages__basic_all)
  - [linux_packages__basic_centos_7](#linux_packages__basic_centos_7)
  - [linux_packages__basic_debian_12](#linux_packages__basic_debian_12)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### linux_packages__basic_all

List, optional.

List of basic packages for all distributions.

#### Default value

```YAML
linux_packages__basic_all:
  - at
  - bash
  - bash-completion
  - bats
  - bonnie++
  - ca-certificates
  - curl
  - gawk
  - git
  - gnupg
  - grep
  - gzip
  - htop
  - iftop
  - iotop
  - jq
  - less
  - lsof
  - man
  - moreutils
  - ncdu
  - net-tools
  - nmap
  - oathtool
  - openconnect
  - openssl
  - parallel
  - parted
  - patch
  - perl
  - procps
  - rclone
  - rsync
  - screen
  - sed
  - sshpass
  - subversion
  - sudo
  - tar
  - tcpdump
  - tcptraceroute
  - telnet
  - time
  - tmux
  - traceroute
  - tree
  - tzdata
  - unzip
  - vim
  - wget
  - zip
  - zsh
  - zsh-syntax-highlighting
```

### linux_packages__basic_centos_7

List, optional.

List of basic packages for specific distribution.

#### Default value

```YAML
linux_packages__basic_centos_7:
  - cronie
  - dejavu-sans-fonts
  - dejavu-sans-mono-fonts
  - dejavu-serif-fonts
  - fontawesome-fonts
  - fuse-sshfs
  - iproute
  - libfaketime
  - openldap-clients
  - openssh-clients
  - ShellCheck
  - sqlite
  - yum-plugin-versionlock
  - yum-utils
```

### linux_packages__basic_debian_12

List, optional.

List of basic packages for specific distribution.

#### Default value

```YAML
linux_packages__basic_debian_12:
  - bat
  - cron
  - faketime
  - fatsort
  - fonts-dejavu
  - fonts-font-awesome
  - fonts-powerline
  - fzf
  - git-filter-repo
  - iproute2
  - ldap-utils
  - locales
  - openssh-client
  - shellcheck
  - sqlite3
  - sshfs
  - zsh-autosuggestions
  - zsh-theme-powerlevel9k
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
