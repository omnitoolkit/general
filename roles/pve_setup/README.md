# Ansible role 'pve_setup'

**Description:** This role configures the setup of a Proxmox VE system.

**Required variables:** None.

**Additional info:** None.

## Table of content

- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
