# Ansible role 'docker_compose'

**Description:** This role configures docker-compose services on the system.

**Required variables:** None.

**Additional info:** None.

Requires docker-compose < 2.0.0 to be installed on the system.

## Table of content

- [Default Variables](#default-variables)
  - [docker_compose__backup_compress](#docker_compose__backup_compress)
  - [docker_compose__backup_cron_day](#docker_compose__backup_cron_day)
  - [docker_compose__backup_cron_hour](#docker_compose__backup_cron_hour)
  - [docker_compose__backup_cron_minute](#docker_compose__backup_cron_minute)
  - [docker_compose__backup_cron_month](#docker_compose__backup_cron_month)
  - [docker_compose__backup_cron_weekday](#docker_compose__backup_cron_weekday)
  - [docker_compose__backup_heartbeat_url](#docker_compose__backup_heartbeat_url)
  - [docker_compose__backup_name](#docker_compose__backup_name)
  - [docker_compose__backup_retention_days](#docker_compose__backup_retention_days)
  - [docker_compose__backup_services_exclude](#docker_compose__backup_services_exclude)
  - [docker_compose__backup_set](#docker_compose__backup_set)
  - [docker_compose__env_vars__*](#docker_compose__env_vars__*)
  - [docker_compose__extra_files__*](#docker_compose__extra_files__*)
  - [docker_compose__files__*](#docker_compose__files__*)
  - [docker_compose__path_dest](#docker_compose__path_dest)
  - [docker_compose__path_src](#docker_compose__path_src)
  - [docker_compose__project_name](#docker_compose__project_name)
  - [docker_compose__reboot_script_set](#docker_compose__reboot_script_set)
  - [docker_compose__set_build](#docker_compose__set_build)
  - [docker_compose__set_dependencies](#docker_compose__set_dependencies)
  - [docker_compose__set_profiles](#docker_compose__set_profiles)
  - [docker_compose__set_pull](#docker_compose__set_pull)
  - [docker_compose__set_recreate](#docker_compose__set_recreate)
  - [docker_compose__set_remove_images](#docker_compose__set_remove_images)
  - [docker_compose__set_remove_orphans](#docker_compose__set_remove_orphans)
  - [docker_compose__set_remove_volumes](#docker_compose__set_remove_volumes)
  - [docker_compose__set_services](#docker_compose__set_services)
  - [docker_compose__set_state](#docker_compose__set_state)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### docker_compose__backup_compress

Boolean, optional.

Set gzip compression for the backup.

#### Default value

```YAML
docker_compose__backup_compress: true
```

### docker_compose__backup_cron_day

String, optional.

Day of the month the cronjob should run.

#### Default value

```YAML
docker_compose__backup_cron_day: '*'
```

### docker_compose__backup_cron_hour

String, optional.

Hour when the cronjob should run.

#### Default value

```YAML
docker_compose__backup_cron_hour: '23'
```

### docker_compose__backup_cron_minute

String, optional.

Minute when the cronjob should run.

#### Default value

```YAML
docker_compose__backup_cron_minute: '30'
```

### docker_compose__backup_cron_month

String, optional.

Month of the year the cronjob should run.

#### Default value

```YAML
docker_compose__backup_cron_month: '*'
```

### docker_compose__backup_cron_weekday

String, optional.

Day of the week the cronjob should run.

#### Default value

```YAML
docker_compose__backup_cron_weekday: '*'
```

### docker_compose__backup_heartbeat_url

String, optional.

Heartbeat URL that will be queried when the backup was successful.

#### Default value

```YAML
docker_compose__backup_heartbeat_url: ''
```

#### Example usage

```YAML
docker_compose__backup_heartbeat_url: https://heartbeat.uptimerobot.com/m794yyyyyyyy-xxxxxxxxxxxxxxx
```

### docker_compose__backup_name

String, optional.

Name of the backup cronjob.

#### Default value

```YAML
docker_compose__backup_name: dc-backup
```

### docker_compose__backup_retention_days

Integer, optional.

Number of days that backup archives are stored.

#### Default value

```YAML
docker_compose__backup_retention_days: 14
```

### docker_compose__backup_services_exclude

List, optional.

List of services to exclude from the backup cronjob.

#### Default value

```YAML
docker_compose__backup_services_exclude: []
```

#### Example usage

```YAML
docker_compose__backup_services_exclude:
  - app
  - database
```

### docker_compose__backup_set

Boolean, optional.

Set cronjob to backup docker volumes.

#### Default value

```YAML
docker_compose__backup_set: false
```

### docker_compose__env_vars__*

List, optional.

Contains dicts, each containing a key-value pair that can be used as variables in docker-compose files.

All variables starting with `docker_compose__env_vars__` are merged and sorted by `key` on runtime for each host. `*` is a placeholder for any string, however it is recommended to use a meaningful and unique name.

If no variables are defined (which is the default), configuration will be skipped.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
docker_compose__env_vars__appserver:
  - key: MY_VAR_1
    value: hello
  - key: MY_VAR_2
    value: world
```

### docker_compose__extra_files__*

List, optional.

Contains dicts, each containing a path to an extra file that will be copied to the host.

Must be a path relative to `docker_compose__path_src`. Parent directories are created automatically. Just before the service configuration, the file can be executed, even multiple times, when `execute:` is specified as a list. Each entry contains the execution parameters or the boolean value `true` if no parameters are needed. Note that `mode:` has to be set to allow execution.

All variables starting with `docker_compose__extra_files__` are merged and sorted by `file` on runtime for each host. `*` is a placeholder for any string, however it is recommended to use a meaningful and unique name.

If no variables are defined (which is the default), configuration will be skipped.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
docker_compose__extra_files__appserver:
  - file: backup/migration.tar.gz
  - file: bin/backup_restore.sh
    mode: "755"
    execute:
      - --help
      - true
```

### docker_compose__files__*

List, required.

Contains dicts, each containing a path to a docker-compose file.

All files are temporarily copied to the target host (even in check mode), parsed with `docker-compose config` including the env vars above and deleted afterwards. The resulting single docker-compose file will be used from then on.

All variables starting with `docker_compose__files__` are merged and sorted by `file` on runtime for each host. `*` is a placeholder for any string, however it is recommended to use a meaningful and unique name.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
docker_compose__files__appserver:
  - file: docker-compose.yml
  - file: docker-compose.prod.yml
```

### docker_compose__path_dest

String, optional.

Path to a directory on the destination host where the files will be saved to.

#### Default value

```YAML
docker_compose__path_dest: /srv/docker
```

### docker_compose__path_src

String, optional.

Path to a directory on the host executing the playbook where the files will be loaded from. Can be a path relative to the playbook's execution directory.

#### Default value

```YAML
docker_compose__path_src: ../../docker
```

### docker_compose__project_name

String, optional.

Name of the compose project. Will be used for container names if no `container_name` is specified and for network names if no `networks` are specified.

#### Default value

```YAML
docker_compose__project_name: docker
```

### docker_compose__reboot_script_set

Boolean, optional.

Set cronjob to start containers after reboot.

#### Default value

```YAML
docker_compose__reboot_script_set: true
```

### docker_compose__set_build

String, optional.

Set the configuration of the docker-compose call.

#### Default value

```YAML
docker_compose__set_build: policy
```

### docker_compose__set_dependencies

Boolean, optional.

Set the configuration of the docker-compose call.

#### Default value

```YAML
docker_compose__set_dependencies: true
```

### docker_compose__set_profiles

List, optional.

Set the configuration of the docker-compose call.

#### Default value

```YAML
docker_compose__set_profiles: []
```

### docker_compose__set_pull

String, optional.

Set the configuration of the docker-compose call.

#### Default value

```YAML
docker_compose__set_pull: policy
```

### docker_compose__set_recreate

String, optional.

Set the configuration of the docker-compose call.

#### Default value

```YAML
docker_compose__set_recreate: auto
```

### docker_compose__set_remove_images

String, optional.

Set the configuration of the docker-compose call.

#### Default value

```YAML
docker_compose__set_remove_images: ''
```

### docker_compose__set_remove_orphans

Boolean, optional.

Set the configuration of the docker-compose call.

#### Default value

```YAML
docker_compose__set_remove_orphans: true
```

### docker_compose__set_remove_volumes

Boolean, optional.

Set the configuration of the docker-compose call.

#### Default value

```YAML
docker_compose__set_remove_volumes: true
```

### docker_compose__set_services

List, optional.

Set the configuration of the docker-compose call.

#### Default value

```YAML
docker_compose__set_services: []
```

### docker_compose__set_state

String, optional.

Set the configuration of the docker-compose call.

#### Default value

```YAML
docker_compose__set_state: present
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
