# Ansible role 'gitlab_groups'

**Description:** This role configures settings, members and variables of GitLab groups.

**Required variables:** See docs.

**Additional info:** None.

## Table of content

- [Default Variables](#default-variables)
  - [gitlab_groups__api_token](#gitlab_groups__api_token)
  - [gitlab_groups__api_url](#gitlab_groups__api_url)
  - [gitlab_groups__default_settings](#gitlab_groups__default_settings)
  - [gitlab_groups__list](#gitlab_groups__list)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### gitlab_groups__api_token

String, optional.

GitLab API token with role `owner`, can either be a personal, group or project token.

#### Default value

```YAML
gitlab_groups__api_token: ''
```

### gitlab_groups__api_url

String, optional.

GitLab API URL, this should normally be the domain of the GitLab instance.

#### Default value

```YAML
gitlab_groups__api_url: https://gitlab.com
```

### gitlab_groups__default_settings

Dict, optional.

Default GitLab group settings, can be overwritten for specific groups.

#### Default value

```YAML
gitlab_groups__default_settings:
  auto_devops_enabled: false
  project_creation_level: maintainer
  subgroup_creation_level: owner
  visibility: public
```

### gitlab_groups__list

List, required.

Contains dicts, each containing a group with settings (optional), members and variables.

Specific settings will be merged with the default ones above.

Current members and variables that are not defined here will be removed.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
gitlab_groups__list:
  - name: examplegroup
    settings:
      visibility: private
    members:
      - access_level: owner
        name: exampleuser
    variables:
      - masked: true
        name: EXAMPLE_VAR
        protected: true
        value: examplevalue
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
