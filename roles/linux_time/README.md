# Ansible role 'linux_time'

**Description:** This role configures the time zone and the time servers.

**Required variables:** None.

**Additional info:** None.

## Table of content

- [Default Variables](#default-variables)
  - [linux_time__servers](#linux_time__servers)
  - [linux_time__zone](#linux_time__zone)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### linux_time__servers

List, optional.

Time servers. You can use public (e.g. from the NTP Pool Project) or private servers.

#### Default value

```YAML
linux_time__servers:
  - 0.de.pool.ntp.org
  - 1.de.pool.ntp.org
  - 2.de.pool.ntp.org
  - 3.de.pool.ntp.org
```

### linux_time__zone

String, optional.

Time zone. List all available values with `timedatectl list-timezones`.

#### Default value

```YAML
linux_time__zone: Europe/Berlin
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
