# Ansible role 'linux_sshd'

**Description:** This role configures the SSH server settings.

**Required variables:** None.

**Additional info:** By default, the role will allow SSH login via password. It is strongly recommended to define the variable `linux_sshd__config__*` (see the example there) and add your ssh key to the `root` role variable `linux_root__authorized_keys__*`.

## Table of content

- [Default Variables](#default-variables)
  - [linux_sshd__config__*](#linux_sshd__config__*)
  - [linux_sshd__config_default_centos_7](#linux_sshd__config_default_centos_7)
  - [linux_sshd__config_default_debian_12](#linux_sshd__config_default_debian_12)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### linux_sshd__config__*

List, optional.

Contains dicts, each containing a key and value for sshd config.

All variables starting with `linux_sshd__config__` are merged and sorted by `key` on runtime for each host. `*` is a placeholder for any string, however it is recommended to use a meaningful and unique name.

If no variables are defined (which is the default), only the default config from below is applied.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
# enforce passwordless login
linux_sshd__config__appserver:
  - key: PasswordAuthentication
    value: "no"
  - key: PermitRootLogin
    value: without-password
```

### linux_sshd__config_default_centos_7

List, optional.

Default sshd config for this distribution. These entries will be merged with the self-defined entries from above.

To disable them, overwrite the variable as shown in the example.

#### Default value

```YAML
linux_sshd__config_default_centos_7:
  - {key: AcceptEnv, value: LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY
      LC_MESSAGES}
  - {key: AcceptEnv, value: LC_IDENTIFICATION LC_ALL LANGUAGE}
  - {key: AcceptEnv, value: LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT}
  - {key: AcceptEnv, value: XMODIFIERS}
  - {key: AuthorizedKeysFile, value: .ssh/authorized_keys}
  - {key: ChallengeResponseAuthentication, value: no}
  - {key: GSSAPIAuthentication, value: yes}
  - {key: GSSAPICleanupCredentials, value: no}
  - {key: HostKey, value: /etc/ssh/ssh_host_rsa_key}
  - {key: HostKey, value: /etc/ssh/ssh_host_ecdsa_key}
  - {key: HostKey, value: /etc/ssh/ssh_host_ed25519_key}
  - {key: Subsystem, value: sftp /usr/libexec/openssh/sftp-server}
  - {key: SyslogFacility, value: AUTHPRIV}
  - {key: UsePAM, value: yes}
  - {key: X11Forwarding, value: yes}
```

#### Example usage

```YAML
# disable default config
linux_sshd__config_default_centos_7: []
```

### linux_sshd__config_default_debian_12

List, optional.

Default sshd config for this distribution. These entries will be merged with the self-defined entries from above.

To disable them, overwrite the variable as shown in the example.

#### Default value

```YAML
linux_sshd__config_default_debian_12:
  - {key: AcceptEnv, value: LANG LC_*}
  - {key: KbdInteractiveAuthentication, value: no}
  - {key: PrintMotd, value: no}
  - {key: Subsystem, value: sftp /usr/lib/openssh/sftp-server}
  - {key: UsePAM, value: yes}
  - {key: X11Forwarding, value: yes}
```

#### Example usage

```YAML
# disable default config
linux_sshd__config_default_debian_12: []
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
