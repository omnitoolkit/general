# Ansible role 'linux_root'

**Description:** This role configures the root password and the files `.ssh/authorized_keys`, `.bashrc` and `.vimrc` in the root folder.

**Required variables:** None.

**Additional info:** By default, the role will skip configuration for root password and `.ssh/authorized_keys` file. It is strongly recommended to define the variables `linux_root__password` and `linux_root__authorized_keys__*`.

## Table of content

- [Default Variables](#default-variables)
  - [linux_root__authorized_keys__*](#linux_root__authorized_keys__*)
  - [linux_root__bashrc](#linux_root__bashrc)
  - [linux_root__password](#linux_root__password)
  - [linux_root__prompt_color](#linux_root__prompt_color)
  - [linux_root__vimrc](#linux_root__vimrc)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### linux_root__authorized_keys__*

List, optional.

Contains strings with authorized_keys entries.

All variables starting with `linux_root__authorized_keys__` are merged and sorted on runtime for each host. `*` is a placeholder for any string, however it is recommended to use a meaningful and unique name.

If no variables are defined (which is the default), configuration will be skipped.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
linux_root__authorized_keys__appserver:
  - ssh-rsa AAAA[...] nobody@localhost
  - ssh-rsa AAAA[...] somebody@localhost
```

### linux_root__bashrc

String, optional.

The content of this variable will be copied to the .bashrc file.

#### Default value

```YAML
linux_root__bashrc: |
  # Ansible managed

  # if not running interactively, don't do anything
  [[ $- != *i* ]] && return

  # source global definitions and completions
  [ -f /etc/bashrc ] && . /etc/bashrc
  [ -f /etc/bash.bashrc ] && . /etc/bash.bashrc
  [ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

  # bash settings
  export PS1='\[\033[{{ linux_root__prompt_color }}\][\u@\h \W]\$\[\033[00m\] '
  shopt -s checkwinsize
  shopt -s expand_aliases
  shopt -s histappend

  # colorful manpages
  export LESS_TERMCAP_mb=$(printf '\e[01;31m')  # enter blinking mode – red
  export LESS_TERMCAP_md=$(printf '\e[01;35m')  # enter double-bright mode – bold, magenta
  export LESS_TERMCAP_me=$(printf '\e[0m')      # turn off all appearance modes (mb, md, so, us)
  export LESS_TERMCAP_se=$(printf '\e[0m')      # leave standout mode
  export LESS_TERMCAP_so=$(printf '\e[01;33m')  # enter standout mode – yellow
  export LESS_TERMCAP_ue=$(printf '\e[0m')      # leave underline mode
  export LESS_TERMCAP_us=$(printf '\e[04;36m')  # enter underline mode – cyan
  export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
                                                # colored GCC warnings and errors

  # aliases
  alias vi='vim'

  # use vim as default editor
  export EDITOR=vim

  {% if (ansible_distribution | lower) == "centos" and ansible_distribution_major_version == "7" %}
  # ignore python warnings in docker-compose
  export PYTHONWARNINGS=ignore
  {% endif %}
```

### linux_root__password

String, optional.

Password of the root user, it is strongly recommended to encrypt this variable.

If the variable is not defined (which is the default), configuration will be skipped.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
linux_root__password: secret
```

### linux_root__prompt_color

String, optional.

Color of the terminal prompt. For more information, check out `man terminfo` and search for "Color Handling".

#### Default value

```YAML
linux_root__prompt_color: 00m
```

#### Example usage

```YAML
linux_root__prompt_color: 01;32m
```

### linux_root__vimrc

String, optional.

The content of this variable will be copied to the .vimrc file.

#### Default value

```YAML
linux_root__vimrc: |
  " Ansible managed
  let &t_SI .= "\<Esc>[?2004h"
  let &t_EI .= "\<Esc>[?2004l"
  inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()
  function! XTermPasteBegin()
    set pastetoggle=<Esc>[201~
    set paste
    return ""
  endfunction
  syntax on
  set background=dark
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
