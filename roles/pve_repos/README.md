# Ansible role 'pve_repos'

**Description:** This role configures the repositories of a Proxmox VE system.

**Required variables:** None.

**Additional info:** None.

## Table of content

- [Default Variables](#default-variables)
  - [pve_repos__ceph_17_type](#pve_repos__ceph_17_type)
  - [pve_repos__ceph_18_type](#pve_repos__ceph_18_type)
  - [pve_repos__pve_type](#pve_repos__pve_type)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### pve_repos__ceph_17_type

String, optional.

Repository type for Proxmox VE. Can be one of "pve-enterprise", "pve-no-subscription" or "pvetest".

#### Default value

```YAML
pve_repos__ceph_17_type: ''
```

### pve_repos__ceph_18_type

#### Default value

```YAML
pve_repos__ceph_18_type: no-subscription
```

### pve_repos__pve_type

#### Default value

```YAML
pve_repos__pve_type: pve-no-subscription
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
