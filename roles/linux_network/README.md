# Ansible role 'linux_network'

**Description:** This role configures network interfaces, DNS, hostname, hosts file, static routing and policy-based routing.

**Required variables:** None.

**Additional info:** By default, the role will guess the configuration for the default interface, ignoring all other interfaces. It is strongly recommended to define the variable `linux_network__interfaces` for each host.

## Table of content

- [Default Variables](#default-variables)
  - [linux_network__backend](#linux_network__backend)
  - [linux_network__dns_search_domain](#linux_network__dns_search_domain)
  - [linux_network__dns_servers](#linux_network__dns_servers)
  - [linux_network__enable_ipv6](#linux_network__enable_ipv6)
  - [linux_network__hosts__*](#linux_network__hosts__*)
  - [linux_network__interfaces](#linux_network__interfaces)
  - [linux_network__name_policy](#linux_network__name_policy)
  - [linux_network__routes__*](#linux_network__routes__*)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### linux_network__backend

String, optional.

Network backend, can be one of `ifupdown2`, `netplan`.

#### Default value

```YAML
linux_network__backend: ifupdown2
```

### linux_network__dns_search_domain

String, optional.

DNS search domain.

#### Default value

```YAML
linux_network__dns_search_domain: local
```

### linux_network__dns_servers

List, optional.

Contains strings with DNS server entries.

#### Default value

```YAML
linux_network__dns_servers:
  - 9.9.9.9
  - 149.112.112.112
```

### linux_network__enable_ipv6

Boolean, optional.

Whether to enable ipv6.

#### Default value

```YAML
linux_network__enable_ipv6: false
```

### linux_network__hosts__*

List, optional.

Contains dicts, each containing an address and a hostname that will be added to the hosts file.

All variables starting with `linux_network__hosts__` are merged and sorted by `address` on runtime for each host. `*` is a placeholder for any string, however it is recommended to use a meaningful and unique name.

If no variables are defined (which is the default), configuration will be skipped.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
linux_network__hosts__appserver:
  - address: 192.168.1.50
    hostname: host50.example.com
  - address: 192.168.1.60
    hostname: host60.example.com
```

### linux_network__interfaces

List, optional.

Contains dicts, each containing the configuration for one interface.

By default, the role will guess the configuration for the default interface, ignoring all other interfaces. Therefore, it is **strongly recommended** to define this variable.

Please note the the example. The interface's `name` is optional and defaults to the `device`. It will also be the name of the routing table for this interface. Exactly one default interface per host is required. The hostname will be `hostname` of the first address of the default interface, or `ansible_fqdn` if no hostname is defined. Additional addresses currently require the same subnet and gateway as the first address.

Virtual interfaces for bonding and bridging can also be defined. This is not available on CentOS 7. The backend `netplan` does not support vlan-aware bridges.

#### Default value

```YAML
linux_network__interfaces:
  - name: MGMT
    addresses:
      - address: "{{ ansible_default_ipv4.address }}/{{ (ansible_default_ipv4.address\
          \ + '/' + ansible_default_ipv4.netmask) | ansible.utils.ipaddr('prefix')\
          \ }}"
        gateway: '{{ ansible_default_ipv4.gateway }}'
        hostname: '{{ ansible_fqdn }}'
    default: true
    device: '{{ ansible_default_ipv4.interface }}'
```

#### Example usage

```YAML
linux_network__interfaces:
  - name: MGMT
    default: true
    device: eth0
    addresses:
      - address: 192.168.1.10/24
        gateway: 192.168.1.1
        hostname: host10.example.com
      - address: 192.168.1.20/24
        gateway: 192.168.1.1
        hostname: host20.example.com

  - name: PROD
    device: eth1
    addresses:
      - address: 192.168.2.30/24
        gateway: 192.168.2.1

  - device: eth2

  - device: eth3

  - bond:
      miimon: 100
      mode: 802.3ad
      slaves:
        - eth2
        - eth3
    device: bond0

  - bridge:
      fd: 0
      ports:
        - bond0
      stp: false
      vids: 2-4094
      vlan_aware: true
```

### linux_network__name_policy

String, optional.

Network name policy as in `NamePolicy` of [systemd.link](https://www.freedesktop.org/software/systemd/man/255/systemd.link.html).

If the variable is not defined (which is the default), configuration will be skipped.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
linux_network__name_policy: mac
```

### linux_network__routes__*

List, optional.

Contains dicts, each containing a network route.

Please note the example. `via` can be used to point to the `name` of the desired interface.

All variables starting with `linux_network__routes__` are merged and sorted by `name` on runtime for each host. `*` is a placeholder for any string, however it is recommended to use a meaningful and unique name.

If no variables are defined (which is the default), configuration will be skipped.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
linux_network__routes__appserver:
  - name: 01 backup
    address: 192.168.3.0/24
    via: MGMT
  - name: 02 monitoring
    address: 192.168.4.0/24
    via: PROD
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
