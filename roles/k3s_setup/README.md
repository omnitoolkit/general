# Ansible role 'k3s_setup'

**Description:** This role installs k3s on the system.

**Required variables:** None.

**Additional info:** None.

## Table of content

- [Default Variables](#default-variables)
  - [k3s_setup__agents](#k3s_setup__agents)
  - [k3s_setup__cluster_cidr](#k3s_setup__cluster_cidr)
  - [k3s_setup__cluster_dns](#k3s_setup__cluster_dns)
  - [k3s_setup__cluster_domain](#k3s_setup__cluster_domain)
  - [k3s_setup__control_plane_no_schedule](#k3s_setup__control_plane_no_schedule)
  - [k3s_setup__control_plane_vip](#k3s_setup__control_plane_vip)
  - [k3s_setup__debug](#k3s_setup__debug)
  - [k3s_setup__interface](#k3s_setup__interface)
  - [k3s_setup__kubeconfig_path](#k3s_setup__kubeconfig_path)
  - [k3s_setup__log](#k3s_setup__log)
  - [k3s_setup__network_policy](#k3s_setup__network_policy)
  - [k3s_setup__proxy_http](#k3s_setup__proxy_http)
  - [k3s_setup__proxy_https](#k3s_setup__proxy_https)
  - [k3s_setup__proxy_no](#k3s_setup__proxy_no)
  - [k3s_setup__psa_default_policy](#k3s_setup__psa_default_policy)
  - [k3s_setup__registries](#k3s_setup__registries)
  - [k3s_setup__servers](#k3s_setup__servers)
  - [k3s_setup__service_cidr](#k3s_setup__service_cidr)
  - [k3s_setup__state](#k3s_setup__state)
  - [k3s_setup__token](#k3s_setup__token)
  - [k3s_setup__version_k3s](#k3s_setup__version_k3s)
  - [k3s_setup__version_kube_vip](#k3s_setup__version_kube_vip)
  - [k3s_setup__version_kube_vip_cloud_provider](#k3s_setup__version_kube_vip_cloud_provider)
  - [k3s_setup__version_kube_vip_website](#k3s_setup__version_kube_vip_website)
  - [k3s_setup__version_longhorn](#k3s_setup__version_longhorn)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### k3s_setup__agents

List, optional.

Contains ansible inventory names that are k3s agents (worker nodes). The entries can be provided manually or with the ansible variable `groups`, e.g. `groups.my_group_name`.

#### Default value

```YAML
k3s_setup__agents: []
```

#### Example usage

```YAML
k3s_setup__agents:
  - k3s-04
  - k3s-05
```

### k3s_setup__cluster_cidr

String, optional.

IPv4/IPv6 network CIDRs to use for pod IPs.

#### Default value

```YAML
k3s_setup__cluster_cidr: ''
```

#### Example usage

```YAML
k3s_setup__cluster_cidr: 10.42.0.0/16
```

### k3s_setup__cluster_dns

String, optional.

IPv4 Cluster IP for coredns service. Should be in `k3s_setup__service_cidr` range.

#### Default value

```YAML
k3s_setup__cluster_dns: ''
```

#### Example usage

```YAML
k3s_setup__cluster_dns: 10.43.0.10
```

### k3s_setup__cluster_domain

String, optional.

Cluster Domain.

#### Default value

```YAML
k3s_setup__cluster_domain: ''
```

#### Example usage

```YAML
k3s_setup__cluster_domain: cluster.local
```

### k3s_setup__control_plane_no_schedule

Boolean, optional.

Sets the taint that kubeadm applies on control plane nodes to restrict placing Pods and allow only specific pods to schedule on them. If this Taint is applied, control plane nodes allow only critical workloads to be scheduled onto them.

#### Default value

```YAML
k3s_setup__control_plane_no_schedule: false
```

### k3s_setup__control_plane_vip

String, required.

Virtual IP address of the k3s API that is set up via `kube-vip`.

#### Default value

```YAML
k3s_setup__control_plane_vip: ''
```

#### Example usage

```YAML
k3s_setup__control_plane_vip: 192.168.100.100
```

### k3s_setup__debug

Boolean, optional.

Turn on debug logs.

#### Default value

```YAML
k3s_setup__debug: false
```

### k3s_setup__interface

Dict, required.

Network interface that is used by k3s. Contains `addresses` (list of dicts with first entry containing `address` and `hostname`) and `device`. Because of this layout, a single element of the variable `linux_network__interfaces` can be used as a source of this setting.

#### Default value

```YAML
k3s_setup__interface: {}
```

#### Example usage

```YAML
k3s_setup__interface:
  addresses:
    - address: 192.168.100.101/24
      hostname: k3s-01
  device: eth0
```

### k3s_setup__kubeconfig_path

String, optional.

The kubeconfig of the k3s cluster will be stored locally in this path. Directories will be created if necessary. If the variable is empty (which is the default), configuration will be skipped.

#### Default value

```YAML
k3s_setup__kubeconfig_path: ''
```

#### Example usage

```YAML
k3s_setup__kubeconfig_path: ~/.kube/config
```

### k3s_setup__log

Boolean, optional.

Log to file.

#### Default value

```YAML
k3s_setup__log: true
```

### k3s_setup__network_policy

Dict, optional.

Contains the yaml content of a network policy file according to the [docs](https://docs.k3s.io/security/hardening-guide#networkpolicies).

#### Default value

```YAML
k3s_setup__network_policy: {}
```

#### Example usage

```YAML
k3s_setup__network_policy:
  kind: NetworkPolicy
  apiVersion: networking.k8s.io/v1
  metadata:
    name: intra-namespace
    namespace: kube-system
  spec:
    podSelector: {}
    ingress:
      - from:
          - namespaceSelector:
            matchLabels:
              name: kube-system
```

### k3s_setup__proxy_http

String, optional.

HTTP_PROXY setting for k3s service.

#### Default value

```YAML
k3s_setup__proxy_http: ''
```

#### Example usage

```YAML
k3s_setup__proxy_http: http://your-proxy.example.com:8888
```

### k3s_setup__proxy_https

String, optional.

HTTPS_PROXY setting for k3s service.

#### Default value

```YAML
k3s_setup__proxy_https: ''
```

#### Example usage

```YAML
k3s_setup__proxy_https: http://your-proxy.example.com:8888
```

### k3s_setup__proxy_no

String, optional.

NO_PROXY setting for k3s service.

#### Default value

```YAML
k3s_setup__proxy_no: ''
```

#### Example usage

```YAML
k3s_setup__proxy_no: 127.0.0.0/8,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
```

### k3s_setup__psa_default_policy

String, optional.

Default policy for Pod Security Admissions. Can be one of `baseline`, `privileged`, `restricted`. See the docs for [k3s](https://docs.k3s.io/security/hardening-guide#pod-security), [PSA](https://kubernetes.io/docs/concepts/security/pod-security-admission) and [PSS](https://kubernetes.io/docs/concepts/security/pod-security-standards)

#### Default value

```YAML
k3s_setup__psa_default_policy: baseline
```

### k3s_setup__registries

Dict, optional.

Contains the yaml content of registries configuration file according to the [docs](https://docs.k3s.io/installation/private-registry).

#### Default value

```YAML
k3s_setup__registries: {}
```

#### Example usage

```YAML
k3s_setup__registries:
  mirrors:
    registry.example.com:
      endpoint:
        - "https://registry.example.com:5000"
```

### k3s_setup__servers

List, required.

Contains ansible inventory names that are k3s servers (control plane and etcd). The entries can be provided manually or with the ansible variable `groups`, e.g. `groups.my_group_name`.

#### Default value

```YAML
k3s_setup__servers: []
```

#### Example usage

```YAML
k3s_setup__servers:
  - k3s-01
  - k3s-02
  - k3s-03
```

### k3s_setup__service_cidr

String, optional.

IPv4/IPv6 network CIDRs to use for service IPs.

#### Default value

```YAML
k3s_setup__service_cidr: ''
```

#### Example usage

```YAML
k3s_setup__service_cidr: 10.43.0.0/16
```

### k3s_setup__state

String, optional.

State of the k3s node, can be one of `present`, `absent`.

#### Default value

```YAML
k3s_setup__state: present
```

### k3s_setup__token

String, required.

Shared secret used to join a server or agent to a cluster.

#### Default value

```YAML
k3s_setup__token: ''
```

#### Example usage

```YAML
k3s_setup__token: secret
```

### k3s_setup__version_k3s

String, required.

Version of `k3s` ([release notes](https://github.com/k3s-io/k3s/releases), [release channels](https://update.k3s.io/v1-release/channels)).

#### Default value

```YAML
k3s_setup__version_k3s: ''
```

#### Example usage

```YAML
k3s_setup__version_k3s: v1.29.3+k3s1
```

### k3s_setup__version_kube_vip

String, required.

Version of `kube-vip` ([release notes](https://github.com/kube-vip/kube-vip/releases)).

#### Default value

```YAML
k3s_setup__version_kube_vip: ''
```

#### Example usage

```YAML
k3s_setup__version_kube_vip: v0.7.2
```

### k3s_setup__version_kube_vip_cloud_provider

String, required.

Version of `kube-vip-cloud-provider` ([release notes](https://github.com/kube-vip/kube-vip-cloud-provider/releases)).

#### Default value

```YAML
k3s_setup__version_kube_vip_cloud_provider: ''
```

#### Example usage

```YAML
k3s_setup__version_kube_vip_cloud_provider: v0.0.9
```

### k3s_setup__version_kube_vip_website

String, required.

Version of `kube-vip-website` ([commits](https://github.com/kube-vip/website/commits)).

#### Default value

```YAML
k3s_setup__version_kube_vip_website: ''
```

#### Example usage

```YAML
k3s_setup__version_kube_vip_website: 244656a
```

### k3s_setup__version_longhorn

String, required.

Version of `longhorn` ([release notes](https://github.com/longhorn/longhorn/releases)).

#### Default value

```YAML
k3s_setup__version_longhorn: ''
```

#### Example usage

```YAML
k3s_setup__version_longhorn: v1.6.1
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
