- name: Disable and stop k3s service.
  ansible.builtin.systemd:
    name: k3s
    enabled: false
    state: stopped
  failed_when: false
  notify: Reboot the server.
#
- name: Reload services.
  ansible.builtin.systemd:
    daemon_reload: true
  changed_when: false
#
- name: Kill all remaining processes and children.
  ansible.builtin.shell: # noqa: risky-shell-pipe
    cmd: |
      ps -e -o pid= -o args= | sed -e 's/^ *//; s/\s\s*/\t/;' | grep -w 'k3s/data/[^/]*/bin/containerd-shim' | cut -f1  \
        | xargs -rtn1 sh -c 'pkill -9 -P $0; kill -9 $0'
  changed_when: k3s_setup__kill_processes.stderr | length > 0
  notify: Reboot the server.
  register: k3s_setup__kill_processes
#
- name: Unmount filesystems.
  ansible.builtin.shell: # noqa: risky-shell-pipe
    cmd: cat /proc/mounts | awk '{print $2}' | sort -r | grep "{{ item }}" | xargs -rtn1 sh -c 'umount -f "$0" && rm -rf "$0"'
  changed_when: k3s_setup__unmount_filesystems.stderr | length > 0
  notify: Reboot the server.
  register: k3s_setup__unmount_filesystems
  with_items:
    - /run/k3s
    - /var/lib/rancher/k3s
    - /var/lib/kubelet/pods
    - /var/lib/kubelet/plugins
    - /run/netns/cni-
#
- name: Delete interfaces.
  ansible.builtin.shell: # noqa: risky-shell-pipe
    cmd: |
      ip netns show 2>/dev/null | grep cni- | awk '{print $1}' | xargs -rtn1 ip netns delete; \
      ip link show 2>/dev/null | grep 'master cni0' | awk '{print $2}' | awk -F '@' '{print $1}' | xargs -rtn1 ip link delete; \
      ip link show 2>/dev/null | awk '{print $2}' | awk -F '@' '{print $1}' \
        | grep 'cni0\|flannel.1\|flannel-v6.1\|kube-ipvs0\|flannel-wg\|flannel-wg-v6' | xargs -rtn1 ip link delete; \
      rm -rf -v /var/lib/cni/ 1>&2
  changed_when: k3s_setup__delete_interfaces.stderr | length > 0
  notify: Reboot the server.
  register: k3s_setup__delete_interfaces
#
- name: Clean iptables.
  ansible.builtin.shell: # noqa: risky-shell-pipe
    cmd: |
      if iptables-save | grep -q 'KUBE-\|CNI-\|FLANNEL\|flannel'; then \
        iptables-save | grep -v KUBE- | grep -v CNI- | grep -iv flannel | iptables-restore -v;
      fi;
      if ip6tables-save | grep -q 'KUBE-\|CNI-\|FLANNEL\|flannel'; then \
        ip6tables-save | grep -v KUBE- | grep -v CNI- | grep -iv flannel | ip6tables-restore -v;
      fi
  changed_when: k3s_setup__clean_iptables.stdout | length > 0
  notify: Reboot the server.
  register: k3s_setup__clean_iptables
#
- name: Remove files and directories.
  ansible.builtin.file:
    dest: "{{ item }}"
    state: absent
  notify: Reboot the server.
  with_items:
    - /etc/rancher/k3s
    - /etc/systemd/system/k3s.service
    - /etc/systemd/system/k3s.service.env
    - /run/flannel
    - /run/k3s
    - /usr/local/bin/crictl
    - /usr/local/bin/ctr
    - /usr/local/bin/k3s
    - /usr/local/bin/kubectl
    - /var/lib/kubelet
    - /var/lib/longhorn
    - /var/lib/rancher/k3s
    - /var/log/containers
    - /var/log/k3s
    - /var/log/pods
