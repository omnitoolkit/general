# Ansible role 'pve_vm'

**Description:** This role configures VMs of a Proxmox VE system.

**Required variables:** None.

**Additional info:** None.

## Table of content

- [Default Variables](#default-variables)
  - [pve_vm__api_host](#pve_vm__api_host)
  - [pve_vm__api_password](#pve_vm__api_password)
  - [pve_vm__api_user](#pve_vm__api_user)
  - [pve_vm__clone_from_id](#pve_vm__clone_from_id)
  - [pve_vm__cores](#pve_vm__cores)
  - [pve_vm__cpu](#pve_vm__cpu)
  - [pve_vm__disks](#pve_vm__disks)
  - [pve_vm__dns_search_domain](#pve_vm__dns_search_domain)
  - [pve_vm__dns_servers](#pve_vm__dns_servers)
  - [pve_vm__id](#pve_vm__id)
  - [pve_vm__memory](#pve_vm__memory)
  - [pve_vm__name](#pve_vm__name)
  - [pve_vm__networks](#pve_vm__networks)
  - [pve_vm__node](#pve_vm__node)
  - [pve_vm__os_type](#pve_vm__os_type)
  - [pve_vm__pci_devices](#pve_vm__pci_devices)
  - [pve_vm__start_on_boot](#pve_vm__start_on_boot)
  - [pve_vm__state](#pve_vm__state)
  - [pve_vm__storage](#pve_vm__storage)
  - [pve_vm__user_name](#pve_vm__user_name)
  - [pve_vm__user_password](#pve_vm__user_password)
  - [pve_vm__user_ssh_keys](#pve_vm__user_ssh_keys)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### pve_vm__api_host

String, required.

Hostname or ip address of Proxmox VE system.

#### Default value

```YAML
pve_vm__api_host: ''
```

#### Example usage

```YAML
pve_vm__api_host: pve-01.example.com
```

### pve_vm__api_password

String, required.

Password of Proxmox VE system.

#### Default value

```YAML
pve_vm__api_password: ''
```

#### Example usage

```YAML
pve_vm__api_password: secret
```

### pve_vm__api_user

String, required.

Username of Proxmox VE system.

#### Default value

```YAML
pve_vm__api_user: ''
```

#### Example usage

```YAML
pve_vm__api_user: root@pam
```

### pve_vm__clone_from_id

Integer, optional.

ID of a vm template from which the vm is created as a full clone.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__clone_from_id: 100
```

### pve_vm__cores

Integer, optional.

Number of cores.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__cores: 2
```

### pve_vm__cpu

String, optional.

Type of cpu.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__cpu: x86-64-v2-AES
```

### pve_vm__disks

List, optional.

Contains dicts, each containing the configuration for a disk.

Caution: when elements are removed from the list, they are no longer managed by this role and need to be manually removed in Proxmox VE system.

`aio` (string) sets the AIO type to use. Choices are `io_uring`, `native`, `threads`.

`backup` (boolean) sets if the disk is included in backups.

`boot_order` (integer) marks the device as bootable and sets the boot order in relation to other bootable devices (ascending order).

`cache` (string) sets how the vm is notified of block write completions. Choices are `directsync`, `none`, `unsafe`, `writeback`, `writethrough`.

`discard` (string) sets if discard / trim requests are relayed to the disk's storage. Choices are `ignore`, `on`.

`format` (string) sets the disk's image format. Choices are `cloop`, `cow`, `qcow`, `qcow2`, `qed`, `raw`, `vmdk`.

`import_from` (string) is an url to a qcow2 image that is imported into a disk. This is only performed on disk creation.

`iothread` (boolean) sets if I/O thread is used for this disk.

`replicate` (boolean) sets if the disk is considered for replication jobs.

`ro` (boolean) sets if the disk is read-only.

`size` (integer, required) sets the disk size in GB.

`ssd` (boolean) sets if the disk is presented to the vm as a ssd.

`storage` (string, required) sets the disk's storage.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__disks:
  - size: 20
    storage: local-lvm
  - backup: true
    boot_order: 10
    cache: none
    discard: false
    format: qcow2
    import_from: https://cloud.example.com/example.qcow2
    iothread: false
    replicate: true
    size: 50
    ssd: false
    storage: local-lvm
```

### pve_vm__dns_search_domain

String, optional.

DNS search domain that is set via cloud-init.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__dns_search_domain: local
```

### pve_vm__dns_servers

List, optional.

DNS servers that are set via cloud-init.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__dns_servers:
  - 9.9.9.9
  - 149.112.112.112
```

### pve_vm__id

Integer, required.

ID of the vm, needs to be unique across the Proxmox VE system.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__id: 100
```

### pve_vm__memory

Integer, optional.

Amount of memory in MB.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__memory: 4096
```

### pve_vm__name

String, required.

Name of the vm, needs to be unique across the Proxmox VE system.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__name: my-vm-100
```

### pve_vm__networks

List, optional.

Contains dicts, each containing the configuration for a network interface.

Caution: when elements are removed from the list, they are no longer managed by this role and need to be manually removed in Proxmox VE system.

`addresses` (list) contains dicts with `address` and `gateway`. Because of this layout, the variable `linux_network__interfaces` can be used as a source of this setting. The first address of every network interface is set as a static configuration via cloud-init. If a network interface does not have an address, dhcp configuration is set. It is not recommended to set this variable for templates.

`boot_order` (integer) marks the device as bootable and sets the boot order in relation to other bootable devices (ascending order).

`host_bridge` (string, required) is the name of the network bridge (on the Proxmox VE system) that is used on the network interface.

`host_firewall` (boolean) sets if the network interface is protected by the firewall of the Proxmox VE system.

`host_mac_address` (string) is the mac address in XX:XX:XX:XX:XX:XX notation of the network interface. If omitted, a random mac address is chosen by Proxmox VE system.

`host_vlan` (integer) is the VLAN id of the network interface.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__networks:
  - host_bridge: vmbr0
  - addresses:
      - address: 10.0.10.101/24
        gateway: 10.0.10.1
    boot_order: 30
    host_bridge: vmbr1
    host_firewall: false
    host_mac_address: BC:24:11:00:00:10
    host_vlan: 10
```

### pve_vm__node

String, required.

Node (normally the first part of the hostname of Proxmox VE system) where the vm is created.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__node: pve-01
```

### pve_vm__os_type

String, optional.

OS type. Can be one of `l24`, `l26`, `other`, `solaris`, `w2k`, `w2k3`, `w2k8`, `win7`, `win8`, `win10`, `win11`, `wvista`, `wxp`.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__os_type: l26
```

### pve_vm__pci_devices

List, optional.

Contains dicts, each containing the configuration for a pci device. It is not recommended to set this variable for templates.

Caution: when elements are removed from the list, they are no longer managed by this role and need to be manually removed in Proxmox VE system.

`boot_order` (integer) marks the device as bootable and sets the boot order in relation to other bootable devices (ascending order).

`pcie` (boolean) tells the Proxmox VE system to use a pcie instead of a pci port.

`rombar` (boolean) makes the firmware rom visible for the guest.

`slot` (string, required) is the name of the slot (on the Proxmox VE system) where the device resides. The syntax is `[domain:]bus:device.function`. If the device has multiple functions, they can be passed through all together by leaving out the function element.

`x_vga` (boolean) marks the device as the primary gpu of the vm.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__pci_devices:
  - boot_order: 40
    pcie: true
    rombar: true
    slot: 0000:01:00
    x_vga: true
```

### pve_vm__start_on_boot

Boolean, optional.

Set to `true` to start the vm at boot of the Proxmox VE system.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__start_on_boot: true
```

### pve_vm__state

String, optional.

Set the state of the vm. Can be one of `restarted`, `started`, `stopped`, `template`.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__state: template
```

### pve_vm__storage

String, required.

Storage for cloud-init, efi and tpm.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__storage: local-lvm
```

### pve_vm__user_name

String, optional.

Name of the user that password and ssh keys are set for via cloud-init.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__user_name: root
```

### pve_vm__user_password

String, optional.

Password that is set via cloud-init for the user specified in `pve_vm__user_name`.

Caution: changes to this setting in existing vms are not detected and therefore not updated.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__user_password: secret
```

### pve_vm__user_ssh_keys

List, optional.

List of ssh keys that is set via cloud-init for the user specified in `pve_vm__user_name`.

#### Default value

```YAML
# undefined
```

#### Example usage

```YAML
pve_vm__user_ssh_keys:
  - ssh-rsa AAAA[...] nobody@localhost
  - ssh-rsa AAAA[...] somebody@localhost
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
