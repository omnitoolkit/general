# Ansible role 'linux_update'

**Description:** This role updates the installed packages and reboots the server if necessary.

**Required variables:** None.

**Additional info:** None.

## Table of content

- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
