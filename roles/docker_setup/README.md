# Ansible role 'docker_setup'

**Description:** This role installs Docker on the system.

**Required variables:** None.

**Additional info:** None.

The installation is performed according to the official [instructions](https://docs.docker.com/engine/install). It is strongly recommended to pin the versions and update on a regular basis. Please refer to the release notes in the description of the variables below.

## Table of content

- [Default Variables](#default-variables)
  - [docker_setup__add_repository](#docker_setup__add_repository)
  - [docker_setup__version_containerd_io](#docker_setup__version_containerd_io)
  - [docker_setup__version_docker_buildx_plugin](#docker_setup__version_docker_buildx_plugin)
  - [docker_setup__version_docker_ce](#docker_setup__version_docker_ce)
  - [docker_setup__version_docker_compose_plugin](#docker_setup__version_docker_compose_plugin)
  - [docker_setup__version_docker_scan_plugin](#docker_setup__version_docker_scan_plugin)
  - [docker_setup__version_yq](#docker_setup__version_yq)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### docker_setup__add_repository

 Boolean, optional.

Controls whether the official Docker repository is added to the package manager. Only used by CentOS 7 hosts.

#### Default value

```YAML
docker_setup__add_repository: true
```

### docker_setup__version_containerd_io

String, optional.

Version of the dist package `containerd.io` ([release notes](https://github.com/containerd/containerd/releases), packages for [CentOS 7](https://download.docker.com/linux/centos/7/x86_64/stable/Packages/), [Debian 12](https://download.docker.com/linux/debian/dists/bookworm/pool/stable/amd64/)). Defaults to the latest version. CentOS 7 requires the release number as a suffix.

#### Default value

```YAML
docker_setup__version_containerd_io: ''
```

### docker_setup__version_docker_buildx_plugin

String, optional.

Version of the dist package `docker-buildx-plugin` ([release notes](https://github.com/docker/buildx/releases), packages for [CentOS 7](https://download.docker.com/linux/centos/7/x86_64/stable/Packages/), [Debian 12](https://download.docker.com/linux/debian/dists/bookworm/pool/stable/amd64/)). Defaults to the latest version. CentOS 7 requires the release number as a suffix.

#### Default value

```YAML
docker_setup__version_docker_buildx_plugin: ''
```

### docker_setup__version_docker_ce

String, optional.

Version of the dist packages `docker-ce`, `docker-ce-cli` and `docker-ce-rootless-extras` ([moby release notes](https://github.com/moby/moby/releases), [Docker release notes](https://docs.docker.com/engine/release-notes), packages for [CentOS 7](https://download.docker.com/linux/centos/7/x86_64/stable/Packages/), [Debian 12](https://download.docker.com/linux/debian/dists/bookworm/pool/stable/amd64/)). Defaults to the latest version. CentOS 7 requires the release number as a suffix.

#### Default value

```YAML
docker_setup__version_docker_ce: ''
```

### docker_setup__version_docker_compose_plugin

String, optional.

Version of the dist package `docker-compose-plugin` ([release notes](https://github.com/docker/compose/releases), packages for [CentOS 7](https://download.docker.com/linux/centos/7/x86_64/stable/Packages/), [Debian 12](https://download.docker.com/linux/debian/dists/bookworm/pool/stable/amd64/)). Defaults to the latest version. CentOS 7 requires the release number as a suffix.

#### Default value

```YAML
docker_setup__version_docker_compose_plugin: ''
```

### docker_setup__version_docker_scan_plugin

String, optional.

Version of the dist package `docker-scan-plugin` ([release notes](https://github.com/docker/scan-cli-plugin/releases), packages for [CentOS 7](https://download.docker.com/linux/centos/7/x86_64/stable/Packages/), [Debian 12](https://download.docker.com/linux/debian/dists/bookworm/pool/stable/amd64/)). Defaults to the latest version. CentOS 7 requires the release number as a suffix.

#### Default value

```YAML
docker_setup__version_docker_scan_plugin: ''
```

### docker_setup__version_yq

String, optional.

Version of the package `yq` ([release notes](https://github.com/mikefarah/yq/releases)). Defaults to the latest version.

#### Default value

```YAML
docker_setup__version_yq: ''
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
