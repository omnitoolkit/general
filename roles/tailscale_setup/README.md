# Ansible role 'tailscale_setup'

**Description:** This role installs Tailscale on the system.

**Required variables:** None.

**Additional info:** None.

## Table of content

- [Default Variables](#default-variables)
  - [tailscale_setup__version](#tailscale_setup__version)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### tailscale_setup__version

String, optional.

Version of the dist package `tailscale` ([changelog](https://tailscale.com/changelog/). Defaults to the latest version.

#### Default value

```YAML
tailscale_setup__version: ''
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
