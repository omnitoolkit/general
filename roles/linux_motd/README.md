# Ansible role 'linux_motd'

**Description:** This role configures the message of the day.

**Required variables:** None.

**Additional info:** None.

## Table of content

- [Default Variables](#default-variables)
  - [linux_motd](#linux_motd)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### linux_motd

String, optional.

Message that will be displayed after login.

#### Default value

```YAML
linux_motd: +++ Ansible managed +++
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
