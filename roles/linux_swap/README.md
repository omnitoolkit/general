# Ansible role 'linux_swap'

**Description:** This role configures the swappiness of the system.

**Required variables:** None.

**Additional info:** None.

## Table of content

- [Default Variables](#default-variables)
  - [linux_swap__swappiness](#linux_swap__swappiness)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### linux_swap__swappiness

String, optional.

How aggressive the kernel will swap memory pages. It can range from 0 to 100, higher means more swappy.

#### Default value

```YAML
linux_swap__swappiness: '10'
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
