# https://wiki.debian.org/NvidiaGraphicsDrivers
# https://forums.developer.nvidia.com/t/280908/55
# https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html
#
# get current version:
#   nvidia-smi --query-gpu=driver_version --format=csv,noheader --id=0
#   modinfo nvidia-current --field version
#   nvcc --version
#   nvidia-ctk --version
#
# get latest version:
#   https://download.nvidia.com/XFree86/Linux-x86_64/latest.txt
#   https://developer.nvidia.com/cuda-toolkit-archive
#   https://github.com/NVIDIA/nvidia-container-toolkit/releases
#   curl -s "https://archlinux.org/packages/extra/x86_64/nvidia/json/" | jq -r ".pkgver"
#   curl -s "https://archlinux.org/packages/extra/x86_64/cuda/json/" | jq -r ".pkgver"
#   curl -s "https://aur.archlinux.org/rpc/v5/info?arg[]=nvidia-container-toolkit" | jq -r ".results[0].Version"
#
# install latest version:
#   apt-get install -y build-essential pkg-config libglvnd-dev mesa-utils wget dkms
#   export NVIDIA_INSTALL_VERSION=$(curl -s "https://archlinux.org/packages/extra/x86_64/nvidia/json/" | jq -r ".pkgver")
#   wget https://us.download.nvidia.com/XFree86/Linux-x86_64/${NVIDIA_INSTALL_VERSION}/NVIDIA-Linux-x86_64-${NVIDIA_INSTALL_VERSION}.run
#   chmod +x ./NVIDIA-Linux-x86_64-${NVIDIA_INSTALL_VERSION}.run
#   ./NVIDIA-Linux-x86_64-${NVIDIA_INSTALL_VERSION}.run --no-x-check --no-cc-version-check --dkms --silent
#   reboot
#
# test:
#  docker run --rm -it --gpus all debian:12 nvidia-smi
#  docker run --rm -it --gpus all docker.io/oguzpastirmaci/gpu-burn:latest 60
#
- name: Update cache.
  ansible.builtin.apt:
    update_cache: true
  changed_when: false
  check_mode: false
#
- name: Execute notified handlers.
  ansible.builtin.meta: flush_handlers
#
- name: Set non-free repository.
  ansible.builtin.apt_repository:
    filename: debian-non-free
    mode: "0644"
    repo: "deb http://deb.debian.org/debian/ {{ ansible_distribution_release }} main contrib non-free non-free-firmware"
    state: present
    update_cache: false
  register: nvidia_driver__set_non_free_repository
#
- name: Update cache.
  ansible.builtin.apt:
    update_cache: true
  changed_when: false
  check_mode: false
#
- name: Install nvidia-driver.
  ansible.builtin.apt:
    name:
      - firmware-misc-nonfree
      - nvidia-driver
      - nvtop
    state: present
  notify: Reboot the server.
  when: not (ansible_check_mode and nvidia_driver__set_non_free_repository.changed)
#
- name: Execute notified handlers.
  ansible.builtin.meta: flush_handlers
#
- name: Install cuda.
  ansible.builtin.apt:
    name:
      - nvidia-cuda-dev
      - nvidia-cuda-toolkit
    state: present
  when: not (ansible_check_mode and nvidia_driver__set_non_free_repository.changed)
#
- name: Create keyrings directory.
  ansible.builtin.file:
    mode: "0755"
    path: /etc/apt/keyrings/
    state: directory
#
- name: Download gpg key.
  ansible.builtin.get_url:
    checksum: sha256:c880576d6cf75a48e5027a871bac70fd0421ab07d2b55f30877b21f1c87959c9
    dest: /etc/apt/keyrings/nvidia-container-toolkit.gpg.armored
    mode: "0644"
    url: https://nvidia.github.io/libnvidia-container/gpgkey
  register: nvidia_driver__download_gpg_key
#
- name: Dearmor gpg key.
  ansible.builtin.shell: # noqa: no-handler risky-shell-pipe
    cmd: cat /etc/apt/keyrings/nvidia-container-toolkit.gpg.armored | gpg --dearmor --output /etc/apt/keyrings/nvidia-container-toolkit.gpg --yes
  changed_when: true
  when: nvidia_driver__download_gpg_key.changed
#
- name: Set nvidia-container-toolkit repository.
  ansible.builtin.apt_repository:
    filename: nvidia-container-toolkit
    mode: "0644"
    repo: deb [signed-by=/etc/apt/keyrings/nvidia-container-toolkit.gpg] https://nvidia.github.io/libnvidia-container/stable/deb/$(ARCH) /
    state: present
    update_cache: false
  register: nvidia_driver__set_nvidia_container_toolkit_repository
#
- name: Update cache.
  ansible.builtin.apt:
    update_cache: true
  changed_when: false
  check_mode: false
#
- name: Add version pinning.
  ansible.builtin.copy:
    content: |
      {% if nvidia_driver__version_nvidia_container_toolkit | length > 0 %}
      Package: nvidia-container-toolkit
      Pin: version {{ (nvidia_driver__version_nvidia_container_toolkit | ansible.builtin.split('-'))[0] }}*
      Pin-Priority: 1000
      {% endif %}
    dest: /etc/apt/preferences.d/nvidia-container-toolkit
    mode: "0644"
#
- name: Install nvidia-container-toolkit.
  ansible.builtin.apt:
    name: nvidia-container-toolkit
    allow_downgrade: true
    state: "{% if nvidia_driver__version_nvidia_container_toolkit | length > 0 %}latest{% else %}present{% endif %}"
  when: not (ansible_check_mode and nvidia_driver__set_nvidia_container_toolkit_repository.changed)
#
- name: Read docker config.
  ansible.builtin.slurp:
    src: /etc/docker/daemon.json
  failed_when: false
  register: nvidia_driver__docker_config
#
- name: Create docker config directory.
  ansible.builtin.file:
    mode: "0755"
    path: /etc/docker/
    state: directory
#
- name: Write docker config.
  ansible.builtin.copy:
    content: |
      {{ nvidia_driver__docker_config.content | default('') | b64decode | default('{}', true) | from_json
        | combine({'runtimes': {'nvidia': {'args': [], 'path': 'nvidia-container-runtime'}}}) | to_nice_json }}
    dest: /etc/docker/daemon.json
    mode: "0644"
  notify: Restart docker.service.
#
- name: Populate service facts.
  ansible.builtin.service_facts:
