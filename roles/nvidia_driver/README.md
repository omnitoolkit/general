# Ansible role 'nvidia_driver'

**Description:** This role configures the nvidia driver.

**Required variables:** None.

**Additional info:** None.

## Table of content

- [Default Variables](#default-variables)
  - [nvidia_driver__version_nvidia_container_toolkit](#nvidia_driver__version_nvidia_container_toolkit)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### nvidia_driver__version_nvidia_container_toolkit

String, optional.

Version of the package `nvidia-container-toolkit` ([release notes](https://github.com/NVIDIA/nvidia-container-toolkit/releases)). Defaults to the latest version.

#### Default value

```YAML
nvidia_driver__version_nvidia_container_toolkit: ''
```



## Dependencies

None.

## License

Apache-2.0

## Author

omnitoolkit contributors
